uniform extern texture ScreenTexture;	

sampler ScreenS = sampler_state
{
	Texture = <ScreenTexture>;	
};

float BlurDistance = 0.003f;
  
float4 PixelShaderFunction(float2 texCoord: TEXCOORD0) : COLOR
{
	float4 Color;

	Color  = tex2D( ScreenS, float2(texCoord.x+BlurDistance, texCoord.y+BlurDistance));
	Color += tex2D( ScreenS, float2(texCoord.x-BlurDistance, texCoord.y-BlurDistance));
	Color += tex2D( ScreenS, float2(texCoord.x+BlurDistance, texCoord.y-BlurDistance));
	Color += tex2D( ScreenS, float2(texCoord.x-BlurDistance, texCoord.y+BlurDistance));

	Color = Color / 4;
	  
	// returned the blurred color
	return Color;
}

technique
{
	pass P0
	{
		PixelShader = compile ps_2_0 PixelShaderFunction();
	}
}