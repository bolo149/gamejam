﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    class UserInterface
    {
        List<Drawable> mHealthMeters = new List<Drawable>();
        Drawable mHealthMeterFull;
        Drawable mHealthMeterHalf;
        Drawable mHealthContainer;
        Vector2 mHealthPos;
        Vector2 mScorePos;
        bool mUpdating = false;

        float mHealth;
        public float Health
        {
            get { return mHealth; }
            set { mHealth = value; }
        }

        int mScore;
        public int Score
        {
            get { return mScore; }
            set { mScore = value; }
        }

        public UserInterface(Vector2 theHealthPos, Vector2 theScorePos)
        {
            //mHealthContainerPos = mHealthPos = theHealthPos;
            mHealthPos = theHealthPos;
            mScorePos = theScorePos;
            mScore = 0;
        }

        public void LoadContent()
        { 
            mHealthContainer = new Drawable(new Animation(false, new int[] { 91 }), mHealthPos);
            mHealthContainer.Scale = 1.0f;

            for (int i = 0; i < 5; i++)
            {
                Vector2 bonePos = mHealthPos;
                bonePos.X += 35*i + 3;
                mHealthMeterFull = new Drawable(new Animation(false, new int[] { 89 }), bonePos);
                mHealthMeterFull.Scale = 1.0f;
                mHealthMeters.Add(mHealthMeterFull);
            }
            //mHealthMeterFull = new Drawable(new Animation(false, new int[] { 89 }), mHealthPos);
            //mHealthMeterHalf = new Drawable(new Animation(false, new int[] { 90 }), mHealthPos);
           
        }

        public void Update(GameTime gameTime, Player thePlayer)
        {
            /*float difference = thePlayer.Health - mHealth; //Will be positive if the player gained, negative if lost
            if (difference < 0)
            {   
                mUpdating = true;
                if (mHealthMeters.ElementAt(mHealthMeters.Count() - 1).forHealth) //If this is true, this item is a half bone
                {
                    mHealthMeters.RemoveAt(mHealthMeters.Count() - 1);
                   
                }
                else //If false, full bone, change to half
                {
                    Vector2 tempPos = mHealthMeters.ElementAt(mHealthMeters.Count() - 1).Position;
                    mHealthMeters.RemoveAt(mHealthMeters.Count() - 1);
                    Drawable halfBone = new Drawable(new Animation(false, new int[] { 90 }), tempPos);
                    halfBone.Scale = 1.0f;
                    halfBone.forHealth = true;  //Half bone not full
                    mHealthMeters.Add(halfBone);
                } 
                mHealth--; //Lose health
            }
            else if (difference > 0)
            {
                mHealth++;
            }
            mUpdating = false;*/


        }

        public void Draw(SpriteBatch batch, GameTime gameTime)
        {
            if (!mUpdating)
            {
                mHealthContainer.draw(batch, Vector2.Zero);

                for (int i = 0; i < mHealthMeters.Count(); i++)
                {
                    mHealthMeters.ElementAt(i).draw(batch, Vector2.Zero);
                }
            }

        }

    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´