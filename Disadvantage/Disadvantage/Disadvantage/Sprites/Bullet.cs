﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    class Bullet : Drawable
    {
        Vector2 mVelocity;
        public float mDamage;
        public bool mToRemove = false;

        public Bullet(Vector2 velocity, Vector2 position, float damage)
            : base()
        {
            mDamage = damage;
            mVelocity = velocity;
            mPosition = position;
            mAnimations.AddLast(new Animation(false, new int[] { 87 }));
            Orient = Functions.VectorToAngle(velocity);
            Origin = new Vector2(0, 0);
            Scale = 4;
        }

        public override void update(Map map, GameTime gametime)
        {
            base.update(map, gametime);
            mPosition.X += mVelocity.X;
            mPosition.Y -= mVelocity.Y;
        }

        public override void draw(SpriteBatch batch, Vector2 offset)
        {
            base.draw(batch, offset);
        }

    }
}
