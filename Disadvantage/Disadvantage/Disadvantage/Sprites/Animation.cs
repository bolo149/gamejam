﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public class Animation
    {
        List<Sprite> mSprites;
        int mCurrentIndex;
        int mTotalSprites;
        double mTimeLeft;
        public bool paused;
        public bool onePlay;
        public bool loop;
        int mPauseFrame;

        double mOverrideWaitTime;
        public double OverrideTime
        {
            get { return mOverrideWaitTime; }
            set { mOverrideWaitTime = value; }
        }

        public Animation(bool loop, int[] spriteIndexes)
        {
            mCurrentIndex = 0;
            mOverrideWaitTime = 0;
            mSprites = new List<Sprite>();
            mTotalSprites = 0;
            mTimeLeft = 0;
            this.loop = loop;
            paused = false;
            onePlay = false;
            mPauseFrame = -1;

            foreach (int id in spriteIndexes)
            {
                addSprite(SpriteManager.get(id));
            }
        }

        public void update(GameTime time, float factor)
        {
            if (paused == true)
                return;

            mTimeLeft -= (time.ElapsedGameTime.TotalMilliseconds*factor);

            if (mTimeLeft <= 0)
            {
                mCurrentIndex += 1;

                if (mCurrentIndex >= mTotalSprites)
                    if (loop) mCurrentIndex = 0;
                    else
                    {
                        mCurrentIndex = mTotalSprites - 1;
                        onePlay = true;
                    }
                if (mOverrideWaitTime > 0)
                    mTimeLeft = mOverrideWaitTime;
                else
                    mTimeLeft = mSprites[mCurrentIndex].FrameTime;
            }

            if (mCurrentIndex == mPauseFrame)
                paused = true;
        }

        public Sprite getSprite() { return mSprites[mCurrentIndex]; }
        public int getWidth() { return getSprite().getWidth(); }
        public int getHeight() { return getSprite().getHeight(); }

        public void reset()
        {
            mCurrentIndex = 0;
            paused = false;
            onePlay = false;
            mTimeLeft = mSprites[mCurrentIndex].FrameTime;
        }

        public void setPauseFrame(int frame)
        {
            mPauseFrame = frame-1;
        }

        public void addSprite(Sprite sprite)
        {
            mSprites.Add(sprite);
            mTotalSprites = mSprites.Count();
        }

        public void setEffects(SpriteEffects effects)
        {
            for (int i = 0; i < mTotalSprites; i++)
                mSprites[i].setEffects(effects);
        }

        public void draw(SpriteBatch batch, Vector2 pos, float orientation, Vector2 origin, float scale)
        {
            Sprite pSprite = mSprites[mCurrentIndex];
            pSprite.draw(batch, pos, orientation, origin, scale);
        }

        public void draw(SpriteBatch batch, Vector2 pos, float orientation, Vector2 origin, float scale, Color c)
        {
            Sprite pSprite = mSprites[mCurrentIndex];
            pSprite.draw(batch, pos, orientation, origin, scale, c);
        }
    }
}

// 　　　　　　　　 　 　 　,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
