﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public class Drawable
    {
        protected LinkedList<Animation> mAnimations;
        protected int mCurrentAnim;
        protected Vector2 mPosition;
        public bool forHealth = false;
        public Vector2 Position
        {
            get { return mPosition; }
            set { mPosition = value; }
        }

        protected Vector2 mOrigin;
        public Vector2 Origin
        {
            get { return mOrigin; }
            set { mOrigin = value; }
        }

        protected float mOrientation;
        public float Orient
        {
            get { return mOrientation; }
            set { mOrientation = value; }
        }

        protected float mScale;
        public float Scale
        {
            get { return mScale; }
            set { mScale = value; }
        }
        
        public Animation getAnimation(int id) { return mAnimations.ElementAt(id); }
        public void addAnimation(Animation anim) { mAnimations.AddLast(anim); }

        public Sprite getSprite() { return getAnimation(mCurrentAnim).getSprite(); }

        public Rectangle getBox()
        {
            return new Rectangle(
                (int)mPosition.X,
                (int)mPosition.Y,
                getSprite().getWidth(),
                getSprite().getHeight());
        }

        public Drawable()
        {
            mCurrentAnim = 0;
            mAnimations = new LinkedList<Animation>();
            mPosition = mOrigin = Vector2.Zero;
            mOrientation = 0;
            mScale = 1.0f;
        }

        public Drawable(Animation defaultAnim, Vector2 pos)
        {
            mAnimations = new LinkedList<Animation>();
            mAnimations.AddLast(defaultAnim);
            mPosition = pos;
        }

        public virtual void update(Map map, GameTime time)
        {
            mAnimations.ElementAt(mCurrentAnim).update(time, 1);
        }

        public virtual void draw(SpriteBatch batch, Vector2 offset)
        {
             
            mAnimations.ElementAt(mCurrentAnim).draw(batch, mPosition - offset, Orient, Origin, Scale);
        }

        public virtual void draw(SpriteBatch batch, Vector2 offset, Color color)
        {
            mAnimations.ElementAt(mCurrentAnim).draw(batch, Position - offset, Orient, Origin, Scale, color);
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
