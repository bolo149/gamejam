﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public class Map
    {
        public Tile StartRoom;
        private Tile[,] mTiles;
        private int mTileSize;
        private int mWidth;
        private int mHeight;
        private int mDifficulty;
        public int Difficulty
        {
            get { return mDifficulty; }
            set { mDifficulty = value; }
        }
        private List<Room> mRooms;
        private bool mItemRoomSpawned;
        private int[] mRoomMaximums = { 3, 6, 8 };
        private int[] mItemsPerDifficulty = { 2, 3 };

        private float[,] mEnemyChanceTable =
        {
            // Difficulty , CHANCE
            { 80, 40, 20 },
            { 40, 80, 50 },
            { 0, 20, 70 }
        };

        public Map( int w, int h, int tileSize, int difficulty )
        {
            mItemRoomSpawned = false;
            mRooms = new List<Room>();
            mDifficulty = difficulty;
            mTileSize = tileSize;
            mWidth = w;
            mHeight = h;
            mTiles = new Tile[mWidth, mHeight];

            for (int x = 0; x < mWidth; x++)
            {
                for (int y = 0; y < mHeight; y++)
                {
                    mTiles[x, y] = new Tile();
                    mTiles[x, y].Position = new Vector2(x*mTileSize, y*mTileSize);
                    mTiles[x, y].Sprite = SpriteManager.get(0);
                    mTiles[x, y].Type = TileType.Solid;
                }
            }
        }

        public Tile getTileAtXY(Vector2 coord)
        {
            if (coord.X < 0 || coord.Y < 0 || coord.X >= mWidth*mTileSize || coord.Y >= mHeight*mTileSize)
                return null;

            coord.X /= mTileSize;
            coord.Y /= mTileSize;
            return mTiles[(int)coord.X, (int)coord.Y];
        }
        
        public Room getCurrentRoom(Vector2 pos)
        {
            foreach (Room room in mRooms)
            {
                if (room.isInRoom(pos))
                    return room;
            }
            return getClosestRoom(pos);
        }

        public Room getClosestRoom(Vector2 pos)
        {
            float lowest = 99999;
            Room chosen = null;
            foreach (Room r in mRooms)
            {
                Vector2 coord = new Vector2(r.Dimensions.X, r.Dimensions.Y);
                float length = (coord - pos).Length();
                if (length < lowest)
                {
                    chosen = r;
                    lowest = length;
                }
            }
            return chosen;
        }

        public void updateDoors(Unit unit, ref double bossTimer)
        {
            Vector2 feet = unit.Position + new Vector2(64, 128);
            Door door = getClosestDoor(feet);
            Room room = getCurrentRoom(feet);
            if (door == null)
                return;

            Vector2 dist = door.position - feet;
            float length = dist.Length();
            if (!door.visited && length < 90)
            {
                door.visited = true;
                Tile newCenter = null;

                Vector2 delta = door.position - feet;
                int x = (int)door.position.X / mTileSize;
                int y = (int)door.position.Y / mTileSize;

                int dir = room.Dimensions.Width / 2;
                if (door.direction == Compass.North)
                {
                    // Door is north
                    y -= dir;
                    //x += 1;
                }
                if (door.direction == Compass.South)
                {
                    // Door is south
                    dir += (dir % 2 == 0) ? 1 : 0;
                    y += dir;
                    //x += 1;
                }

                if (door.direction == Compass.East)
                {
                    // Door is east
                    dir += (dir % 2 == 0) ? 1 : 0;
                    x += dir;
                    //y += 1;
                }
                if (door.direction == Compass.West)
                {
                    // Door is west
                    x -= dir;
                    //y -= 1;
                }

                newCenter = mTiles[x, y];
                if (getTileAtXY(newCenter.Position).Type == TileType.Solid)
                {
                    bossTimer += 6000;
                    GenerateRoom(newCenter, 20, 20, door);
                }
            }

            door.offset = ((120 - length < 0) ? 0 : 120 - (int)length);
            if (door.offset > 64) door.offset = 64;
        }

        public Tile[] getAdjacentTiles( Tile tile )
        {
            Tile[] indices = new Tile[8];

	        const int NUM_DIRS = 8;
	        int[] xMods = {	0,	1,	1,	1,	0,	-1,	-1,	-1 };
	        int[] yMods = { -1, -1,	0,	1,	1,	 1,	 0,	-1 };

            Vector2 pos = tile.Position;

	        for( int i=0; i<NUM_DIRS; i++ )
            {
                pos = tile.Position;
                pos.X += xMods[i] * mTileSize;
                pos.Y += yMods[i] * mTileSize;

                indices[i] = getTileAtXY(pos);
	        }
	        return indices;
        }

        public Door getClosestDoor(Vector2 pos)
        {
            float lowest = 99999;
            Door chosen = null;
            foreach (Room r in mRooms)
            {
                if (r.Doors.Count <= 0)
                    continue;
                foreach (Door door in r.Doors)
                {
                    float length = (door.position - pos).Length();
                    if (length < lowest)
                    {
                        chosen = door;
                        lowest = length;
                    }
                }
            }
            return chosen;
        }

        public void GenerateMap()
        {
            StartRoom = getTileAtXY(new Vector2((mWidth*mTileSize) / 2, (mHeight*mTileSize) / 2));
            GenerateRoom(StartRoom, 20, 20);
            
        }

        public void GenerateRoom(Tile center, int w, int h, Door lastDoor = null)
        {
            /* Indices:
                7   0   1
                6       2
                5   4   3
            */
            int i, n;
            int x = (int)center.Position.X / mTileSize;
            int y = (int)center.Position.Y / mTileSize;
            Tile index;
            // Plot floor tiles
            Vector2 topLeft = new Vector2(center.Position.X - w / 2, center.Position.Y - h / 2);
            for (i = x - (w / 2); i < x + (w / 2); i++)
            {
                for (n = y - (h / 2); n < y + (h / 2); n++)
                {
                    index = mTiles[i, n];
                    if (index.Type == TileType.Solid)
                    {
                        index.Sprite = SpriteManager.get(28);
                        index.Type = TileType.Floor;
                    }
                }
            }
            Room room = new Room(new Rectangle((int)topLeft.X, (int)topLeft.Y, w, h));

            int[] wallIndices = { 18, 13, 17, 11, 15, 12, 16, 14 };

            //Iterate again, add walls
            for (i = x - (w / 2); i < x + (w / 2); i++)
            {
                for (n = y - (h / 2); n < y + (h / 2); n++)
                {
                    index = mTiles[i, n];

                    Tile[] indices = getAdjacentTiles(index);
                    for (int o = 0; o < 8; o+=2)
                    {
                        if ( indices[o].Type != TileType.Null && indices[o].Type == TileType.Solid ) // Has not been touched yet
                        {
                            indices[o].Type = TileType.Wall;
                            indices[o].Sprite = SpriteManager.get( wallIndices[o] );
                        }
                    }
                }
            }
            //last iterate adds corners
            for (i = x - (w / 2); i < x + (w / 2); i++)
            {
                for (n = y - (h / 2); n < y + (h / 2); n++)
                {
                    index = mTiles[i, n];

                    Tile[] indices = getAdjacentTiles(index);
                    for (int o = 1; o < 8; o += 2)
                    {
                        if (indices[o].Type != TileType.Null && indices[o].Type == TileType.Solid) // Has not been touched yet
                        {
                            indices[o].Type = TileType.Wall;
                            indices[o].Sprite = SpriteManager.get(wallIndices[o]);
                        }
                    }
                }
            }
            // DOOR GENERATION
            Compass lastDoorDir = Compass.Null;
            int maxDoors = 4;
            if (lastDoor != null)
            {
                lastDoorDir = lastDoor.direction - 2;
                if (lastDoorDir < 0)
                    lastDoorDir += 4;
                maxDoors--;
                room.addDoor(lastDoor);
            }
            int numDoors = 0;
            for (int d = 0; d < maxDoors; d++)
            {
                if(Functions.random.NextDouble() > 0.6 && mRooms.Count + numDoors < mRoomMaximums[mDifficulty])
                    numDoors++;
            }
            /* You've reached a dead end.
             * check if number of rooms .
            */
            if (numDoors == 0)
            {
                if (!mItemRoomSpawned && mRooms.Count >= mRoomMaximums[mDifficulty])
                {
                    mItemRoomSpawned = true;
                    // spawn item room
                    if (mDifficulty < 2)
                    {
                        Vector2 pos = new Vector2(room.Dimensions.X, room.Dimensions.Y);
                        CollectibleManager.newCollectible(pos, (CollectibleType)mItemsPerDifficulty[mDifficulty]);
                    }
                }
                else if(mRooms.Count < mRoomMaximums[mDifficulty])
                {
                    bool allVisited = true;
                    foreach (Room r in mRooms)
                        foreach (Door d in r.Doors)
                            if (!d.visited) allVisited = false;
                    if (allVisited)
                        numDoors++;
                }
            }

            List<Compass> doorSides = new List<Compass>();

            for (int d = 0; d < numDoors; d++ )
            {
                Vector2 nextRoomCoord = Vector2.Zero;
                int sideChoice = -1;
                bool keepRunning = true;
                while (keepRunning)
                {
                    sideChoice = Functions.random.Next(0, 4);
                    keepRunning = false;
                    foreach (int side in doorSides)
                        if (sideChoice == side)
                            keepRunning = true;
                    if ((Compass)sideChoice == lastDoorDir)
                        keepRunning = true;
                    if (!keepRunning)
                        doorSides.Add((Compass)sideChoice);
                }

                // MAKE A DOOR SON
                room.makeDoor(ref mTiles, (Compass)sideChoice);
            }

            //add grates next to walls
            for (i = x - (w / 2); i < x + (w / 2); i++)
            {
                for (n = y - (h / 2); n < y + (h/2); n++)
                {
                    Tile tile = mTiles[i, n];
                    if (tile.Type == TileType.Null) continue;

                    bool hasWalls = false;
                    Tile[] indices = getAdjacentTiles(tile);
                    for (int o = 0; o < 8; o++)
                    {
                        if (indices[o].Type != TileType.Null)
                        {
                            if (indices[o].Type == TileType.Wall)
                                hasWalls = true;

                            if (indices[o].Type == TileType.Door)
                            {
                                hasWalls = false;
                                break;
                            }
                        }
                    }
                    if (hasWalls)
                    {
                        tile.Type = TileType.Floor;
                        tile.Sprite = SpriteManager.get(29);
                    }
                }
            }

            if (mRooms.Count > 1)
            {
                int numUnits = Functions.random.Next(2, 12);
                for (int u = 0; u < numUnits; u++)
                {
                    Tile enemyTile = getRandomTile(room, true);
                    Enemy enemy = new Enemy();
                    bool chosenEnemy = false;
                    for (int e = 0; e < 3; e++)
                    {
                        if (chosenEnemy) continue;
                        if (Functions.random.Next(0, 100) > mEnemyChanceTable[e, mDifficulty])
                        {
                            chosenEnemy = true;
                            enemy.Type = (EnemyType)e;
                        }
                    }

                    enemy.LoadContent();
                    enemy.Position = room.getRandomPoint();
                    UnitManager.add(enemy);
                }
            }

            mRooms.Add(room);
        }

        public Tile getRandomTile(Room room, bool forceSafe = false)
        {
            Tile tile;
            do
            {
                int randX = Functions.random.Next(room.Dimensions.X, room.Dimensions.X + (room.Dimensions.Width*Functions.TileSize));
                int randY = Functions.random.Next(room.Dimensions.Y, room.Dimensions.Y + (room.Dimensions.Height * Functions.TileSize));
                tile = getTileAtXY(new Vector2(randX, randY));
            } while (tile.Type == TileType.Solid && forceSafe);
            return tile;
        }

        public void constrain(ref Camera camera)
        {
            Vector2 pos = camera.Position;
            if (pos.X <= Functions.ScreenWidth / 2) pos.X = Functions.ScreenWidth / 2;
            if (pos.X >= (mWidth * mTileSize) - Functions.ScreenWidth / 2)
                pos.X = (mWidth * mTileSize) - Functions.ScreenWidth / 2;
            if (pos.Y <= Functions.ScreenHeight / 2) pos.Y = Functions.ScreenHeight / 2;
            if (pos.Y >= (mHeight * mTileSize) - Functions.ScreenHeight / 2)
                pos.Y = (mHeight * mTileSize) - Functions.ScreenHeight / 2;
            camera.Position = pos;
        }

        public void draw(SpriteBatch batch, Camera camera)
        {
            float buffer = 64;
            for(int x = 0; x < mWidth; x++)
            {
                for (int y = 0; y < mHeight; y++)
                {
                    Tile tile = mTiles[x, y];
                    if (tile.Position.X > camera.Position.X - Functions.ScreenWidth / 2 - buffer
                        && tile.Position.X < camera.Position.X + Functions.ScreenWidth / 2 + buffer)
                        if (tile.Position.Y > camera.Position.Y - Functions.ScreenHeight / 2 - buffer
                            && tile.Position.Y < camera.Position.Y + Functions.ScreenHeight / 2 + buffer)
                            tile.Sprite.draw(batch, tile.Position - camera.viewport(), 0, Vector2.Zero, 1);
                }
            }
            foreach (Room room in mRooms)
                room.draw(batch, camera);
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
