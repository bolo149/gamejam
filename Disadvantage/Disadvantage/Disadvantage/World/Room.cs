﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public class Tile
    {
        public Vector2 Position;
        public Sprite Sprite;
        public TileType Type;
    }

    public class Door
    {
        public Vector2 position;
        public Tile[] tiles;
        public int offset;
        public bool vertical;
        public bool visited;
        public Compass direction;
    }

    public class Room
    {
        public Rectangle Dimensions;
        public List<Door> Doors;

        public Room(Rectangle dimension)
        {
            dimension.X += Functions.TileSize;
            dimension.Y += Functions.TileSize;
            Dimensions = dimension;
            Doors = new List<Door>();
        }

        public bool isInRoom(Vector2 pos)
        {
            Rectangle box = Dimensions;
            if (pos.X >= box.X - getWidth() / 2 && pos.X <= box.X + getWidth() / 2)
                if (pos.Y >= box.Y - getHeight() / 2 && pos.Y <= box.Y + getHeight() / 2)
                    return true;
            return false;
        }

        public int getWidth() { return Dimensions.Width * Functions.TileSize; }
        public int getHeight() { return Dimensions.Height * Functions.TileSize; }

        public void makeDoor(ref Tile[,] map, Compass direction)
        {
            Door door = new Door();
            door.direction = direction;
            door.offset = 0;
            door.vertical = (direction == Compass.East || direction == Compass.West) ? true : false;
            door.visited = false;

            int x = (int)Dimensions.X / 64;
            int y = (int)Dimensions.Y / 64;

            door.tiles = new Tile[2];
            if(direction == Compass.North)
            {
                door.tiles[0] = map[x - 1, y - (Dimensions.Height / 2) - 1];
                door.tiles[1] = map[x, y - (Dimensions.Height / 2) - 1];
                door.tiles[0].Sprite = SpriteManager.get(21);
                door.tiles[1].Sprite = SpriteManager.get(23);
            }
            if (direction == Compass.South)
            {
                door.tiles[0] = map[x - 1, y + (Dimensions.Height / 2)];
                door.tiles[1] = map[x, y + (Dimensions.Height / 2)];
                door.tiles[0].Sprite = SpriteManager.get(34);
                door.tiles[1].Sprite = SpriteManager.get(35);
            }
            if (direction == Compass.West)
            {
                door.tiles[0] = map[x - (Dimensions.Width / 2) - 1, y - 1];
                door.tiles[1] = map[x - (Dimensions.Width / 2) - 1, y];
                door.tiles[0].Sprite = SpriteManager.get(25);
                door.tiles[1].Sprite = SpriteManager.get(33);
            }
            if (direction == Compass.East)
            {
                door.tiles[0] = map[x + (Dimensions.Width / 2), y - 1];
                door.tiles[1] = map[x + (Dimensions.Width / 2), y];
                door.tiles[0].Sprite = SpriteManager.get(26);
                door.tiles[1].Sprite = SpriteManager.get(32);
            }

            if(door.vertical)
                door.position = door.tiles[1].Position + new Vector2(64/2, 0);
            else
                door.position = door.tiles[1].Position + new Vector2(0, 64/2);

            door.tiles[0].Type = TileType.Door;
            door.tiles[1].Type = TileType.Door;
            Doors.Add(door);
        }

        public Vector2 getRandomPoint()
        {
            Vector2 topLeft = new Vector2(Dimensions.X - getWidth()/2,
                Dimensions.Y - getHeight()/2);
            Vector2 pos = Vector2.Zero;
            while (!isInRoom(pos))
            {
                pos.X = Functions.random.Next((int)topLeft.X + getWidth() / 4, (int)(topLeft.X + getWidth() / 2));
                pos.Y = Functions.random.Next((int)topLeft.Y + getHeight() / 4, (int)(topLeft.Y + getHeight() / 2));
            }
            pos += new Vector2(-64, -64);
            return pos;
        }
        
        public bool hasDoor(Compass dir)
        {
            foreach (Door door in Doors)
            {
                if (door.direction == dir)
                    return true;
            }
            return false;
        }

        public Door getDoor(Compass dir)
        {
            foreach (Door door in Doors)
            {
                if (door.direction == dir)
                    return door;
            }
            return new Door();
        }

        public void addDoor(Door door)
        {
            Doors.Add(door);
        }

       /* public Door getClosestDoor(Vector2 pos)
        {
            if (Doors.Count <= 0)
                return null;

            float lowest = 99999;
            int index = -1;
            int i = 0;
            foreach (Door door in Doors)
            {
                Vector2 doorPos = door.tiles[1].Position;
                float length = (doorPos - pos).Length();
                if (length < lowest)
                {
                    index = i;
                    lowest = length;
                }
                i++;
            }
            if (index < 0)
                return null;
            return Doors[index];
        }*/

        public void draw(SpriteBatch batch, Camera camera)
        {
            foreach (Door door in Doors)
            {
                int i;
                for (i = 0; i < 2; i++)
                {
                    Vector2 pos = door.tiles[i].Position;
                    SpriteManager.get(28).draw(batch, pos - camera.viewport(), 0, Vector2.Zero, 1);
                    if (door.vertical)
                        pos.Y -= door.offset * ((i > 0) ? -1 : 1);
                    else
                        pos.X -= door.offset * ((i > 0) ? -1 : 1);
                    door.tiles[i].Sprite.draw(batch, pos - camera.viewport(), 0, Vector2.Zero, 1);
                }
            }
        }
    }

    public enum TileType
    {
        Null = -1,
        Solid = 0,
        Wall,
        Floor,
        Door
    }

    public enum Compass
    {
        Null = -1,
        North = 0,
        East,
        South,
        West
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
