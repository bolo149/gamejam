﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Disadvantage
{
    class Player : Unit
    {
        private Ranged mGun;
        private Tazer mTazer;

        public bool mIsJetpackEquipped = false;
        public bool mIsGunEquipped = false;

        public float Health
        {
            get { return mHealth; }
            set { mHealth = value; }
        }

        public float mTargetHeightPos;
        public float mJumpHeight;
        private Drawable mSpriteShadow;
        private Vector2 lastStickDirection;

        Animation mUsingItem;
        public Player()
        {
            mSpeed = 5;
            IsInAir = false;
            mJumpHeight = 90;
            lastStickDirection = Vector2.Zero;
        }

        public void LoadContent()
        {
            base.LoadContent(new int[] { 3, 4 }, new int[] {2});
            mAnimations.AddLast(new Animation(true, new int[] { 36, 37 }));
            mAnimations.AddLast(new Animation(false, new int[] { 1 }));
            mAnimations.AddLast(new Animation(false, new int[] {6, 7}));
            mGun = new Ranged(5, 250, 79);
            mTazer = new Tazer(2.5f);
            mSpriteShadow = new Drawable(new Animation(false, new int[] { 38 }), mPosition);
            mSpriteShadow.Scale = 1.0f;
        }

        public override void update(Map map, GameTime time)
        {
            base.update(map, time);
            mTazer.update(time);
            if(mIsGunEquipped)
                mGun.update(time);
            
            if (Velocity.Y > 0)
            {
                if (!mIsAction)
                    mCurrentAnim = 2;
                mTargetHeightPos -= Velocity.Y;
            }
            else if (Velocity.Y < 0)
            {
                mTargetHeightPos -= Velocity.Y;
            }

            if (IsInAir)
            {
                mPosition.Y = MathHelper.SmoothStep(mPosition.Y, mPosition.Y - mJumpHeight, 0.07f);
                mScale = MathHelper.SmoothStep(mScale, 1.3f, 0.07f);
                mSpriteShadow.Scale = MathHelper.SmoothStep(mSpriteShadow.Scale, 1.3f, 0.07f);
                Vector2 tmpVect = mSpriteShadow.Position;
                tmpVect.Y = MathHelper.SmoothStep(tmpVect.Y, (mPosition.Y + getSprite().getHeight() + mJumpHeight), 0.05f);
                mSpriteShadow.Position = tmpVect;
                if (mTargetHeightPos > mPosition.Y)
                {
                    endJump();
                }
                
            }
            else
            {
                if (mTargetHeightPos > mPosition.Y)
                {
                    mPosition.Y = MathHelper.SmoothStep(mPosition.Y, mPosition.Y + mJumpHeight, 0.07f);
                    mScale = MathHelper.SmoothStep(mScale, 1.0f, 0.07f);
                    mSpriteShadow.Scale = MathHelper.SmoothStep(mSpriteShadow.Scale, 1.0f, 0.07f);
                    Vector2 tmpVect = mSpriteShadow.Position;
                    tmpVect.Y = MathHelper.SmoothStep(tmpVect.Y, (mPosition.Y + getSprite().getHeight()), 0.05f);
                }
            }

            if (!mIsAction)
            {
                if (mVelocity == Vector2.Zero)
                {
                    mCurrentAnim = 0;
                }
                else mCurrentAnim = 1;
            }
            else if (getAnimation(mCurrentAnim).onePlay)
            {
                mCurrentAnim = 0; //If the anim is only meant to play once, set back to idle
                mIsAction = false;
            }
        }

        public void handleInput(GamePadState state)
        {
            Vector2 dir = state.ThumbSticks.Right;
//             dir.X = (float)Math.Ceiling(dir.X);
//             dir.Y = (float)Math.Ceiling(dir.Y);
            mTazer.InUse = false;
            mGun.InUse = false;
            if (state.Triggers.Left != 0)
            {
                // melee
                if (dir != Vector2.Zero && !mTazer.isOnCooldown())
                {
                    mTazer.Orient = Functions.VectorToAngle(dir);

                    Vector2 pos = mPosition + new Vector2(64, 64);
                    Vector2 direction = Functions.AngleToVector(mTazer.Orient);
                    //direction.Normalize();
                    pos += direction * 120;
                    ParticleManager.addEmitter(new LingerEmitter(pos, 500, new Color(255, 255, 0)));
                    SoundManager.playSound(SoundManager.SoundId.TAZER_SOUND);
                    // Check if pos collides with enemies
                    for (int i = 0; i < UnitManager.getNumUnits();i++ )
                    {
                        Unit unit = UnitManager.get(i);
                        if (Collision.pointCollision(pos, unit))
                        {
                            // hurt unit
                            unit.hit(mTazer.Damage);
                            ParticleManager.addEmitter(new RadialBurst(pos, 290, new Color(255,255,0)));
                            mTazer.cooldown();
                        }
                    }
                }
                else
                    mTazer.Orient = Functions.VectorToAngle(lastStickDirection);
                mTazer.InUse = true;
            }
            else if (state.Triggers.Right != 0)
            {
                //ranged

                if (mIsGunEquipped)
                {
                    if (dir != Vector2.Zero)
                    {
                        if (dir.X < 0)
                            mGun.getSprite().setEffects(SpriteEffects.FlipHorizontally);
                        else
                            mGun.getSprite().setEffects(SpriteEffects.None);
                        mGun.Orient = Functions.VectorToAngle(dir);
                        dir.Normalize();
                        fireGun(dir);
                    }
                    else
                        mGun.Orient = Functions.VectorToAngle(lastStickDirection);
                    mGun.InUse = true;
                }
            }

            if (dir != Vector2.Zero)
                lastStickDirection = dir;
        }

        public override void draw(SpriteBatch batch, Vector2 offset)
        {
            if (mTazer.InUse)
                mTazer.draw(batch, mPosition - offset + new Vector2(64, 64));
            else if (mGun.InUse)
                mGun.draw(batch, mPosition - offset + new Vector2(64, 64));
            base.draw(batch, offset);
            mSpriteShadow.draw(batch, offset);
        }

        public override void attack()
        {
            base.attack();
            mCurrentAnim = 3;
            getAnimation(mCurrentAnim).reset();
            mIsAction = true;
        }

        public void jetpack()
        {
            mCurrentAnim = 4;
            getAnimation(mCurrentAnim).reset();
            mIsAction = true;
            jump();
        }


        public void jump()
        {
            mTargetHeightPos = mPosition.Y - mJumpHeight;
            IsInAir = true;
        }

        public void endJump()
        {
            IsInAir = false;
            mTargetHeightPos = mPosition.Y + mJumpHeight;
        }

        public void fireGun(Vector2 bulletDirection)
        {
            bulletDirection *= 10;
            Vector2 offset= Vector2.Zero;
            offset.Y = lastStickDirection.Y * -64;
            offset.X = lastStickDirection.X * 64;

            if (mIsGunEquipped)
            {
                mGun.fire(bulletDirection, (mPosition + new Vector2(64, 64) + offset));
               // ParticleManager.addEmitter( new RadialBurst((mPosition + new Vector2(64, 64) + offset), 200, Color.Red) );
            }
        }

        public void pickupItem(CollectibleType item)
        {
            switch (item)
            {
                case CollectibleType.HEALTHBONE:
                    mHealth += 0.5f;
                    break;
                case CollectibleType.JETPACK:
                    mIsJetpackEquipped = true;
                    break;
                case CollectibleType.LASERGUN:
                    mIsGunEquipped = true;
                    break;
                case CollectibleType.SPECIALGUN:
                    mIsGunEquipped = false;
                    mGun = new Ranged(25, 250, 88);
                    mIsGunEquipped = true;
                    break;
            }

        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
