﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Disadvantage
{
    class Boss : Enemy
    {
        private bool mIsStunned;
        private float mStunDuration;
        private bool mIsFleeing;

        public Boss()
        {
            mCurrentAnim = 1;
            mSpeed = 3;
            mIsFleeing = false;
        }

        public override void LoadContent()
        {
            switch (mType)
            {
                case EnemyType.CHARGING_BOSS:
                    base.LoadContent(new int[] { 91, 92, 93 }, new int[] { 93 });
                    mHealth = 100;
                    break;

                case EnemyType.FLYING_BOSS:
                    base.LoadContent(new int[] { 49, 50, 51 }, new int[] { 50 });
                    mHealth = 100;
                    break;

                case EnemyType.FINAL_BOSS:
                    base.LoadContent(new int[] { 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64 }, new int[] { 52 });
                    mHealth = 100;
                    break;
            };

        }

        public override void update(Map map, GameTime time)
        {
            base.update(map, time);
            switch (mType)
            {
                case EnemyType.CHARGING_BOSS:
                    if (!mIsStunned)
                    {
                        BossCharge(mTarget, map, 0.01f);
                    }
                    else
                        mStunDuration -= (float)time.ElapsedGameTime.TotalSeconds;
                    break;

                case EnemyType.FLYING_BOSS:
                    if (mIsFleeing)
                    {
                        flee(mTarget, 0.05f);
                        Vector2 direction = mTarget.Position - mPosition;
                        float distance = direction.Length();
                        if (distance > 400)
                        {
                            mIsFleeing = !mIsFleeing;
                        }
                    }
                    else
                    {
                        if (mTarget == null) return;
                        seek(mTarget, 0.02f);
                        Vector2 direction = mTarget.Position - mPosition;
                        float distance = direction.Length();
                        if (distance < 20)
                        {
                            mIsFleeing = !mIsFleeing;
                        }
                    }
                    break;

                case EnemyType.FINAL_BOSS:

                    break;
            };

            if (mHealth <= 0)
            {
                Dead = true;
            }

            if (mStunDuration <= 0)
            {
                mIsStunned = false;
                mStunDuration = 0;
            }
        }

        public void seek(Unit target, float timeToTarget)
        {
            if (mTarget == null) return;
            Vector2 vel = target.Position - mPosition;

            vel.Normalize();
            vel *= mSpeed;

            mPosition += vel;
        }

        public void flee(Unit target, float timeToTarget)
        {
            if (mTarget == null) return;
            Vector2 vel = mPosition - target.Position;

            vel.Normalize();
            vel *= mSpeed;

            mPosition += vel;
        }

        public void BossCharge(Unit target, Map map, float timeToTarget)
        {
            if (mTarget == null) return; ;
            Vector2 direction = target.Position - mPosition;
            float distance = direction.Length();
            Vector2 targetPos;
            if (mPosition.X < (target.Position.X + 64) && mPosition.X > (target.Position.X - 64))
            {
                mPosition.Y = MathHelper.Lerp(mPosition.Y, target.Position.Y, (timeToTarget + 0.03f));
            }
            else if (mPosition.Y < (target.Position.Y + 64) && mPosition.Y > (target.Position.Y - 64))
            {
                mPosition.X = MathHelper.Lerp(mPosition.X, target.Position.X, timeToTarget + 0.03f);
            }
            else
            {
                targetPos.X = ((float)Functions.random.Next(-1, 2) * 280) + mPosition.X;
                targetPos.Y = ((float)Functions.random.Next(-1, 2) * 280) + mPosition.Y;
                mPosition.Y = MathHelper.Lerp(mPosition.Y, targetPos.Y, timeToTarget);
                mPosition.X = MathHelper.Lerp(mPosition.X, targetPos.X, timeToTarget);
            }
            foreach(Tile t in map.getAdjacentTiles(map.getTileAtXY(mPosition)))
            {
                if (t.Type == TileType.Wall)
                {
                    //Stun them
                    stun(1.5f);
                }
            }
        }

        public void stun(float duration)
        {
            mIsStunned = true;
            mStunDuration = duration;
        }
    }
}
