﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public enum EnemyType
    {
        FLYING_ENEMY = 0,
        EXPLODING_ENEMY,
        CHARGING_ENEMY,
        FLYING_BOSS,
        CHARGING_BOSS,
        FINAL_BOSS,
    }
    class Enemy:Unit
    {
        private float mMaxSpeed;
        private float mMaxAcceleration;
        protected EnemyType mType;
        private bool mIsExploding;
        private TimeSpan mTimeTillExplosion;
        private bool mDirOne;
        public EnemyType Type
        {
            get { return mType; }
            set { mType = value; }
        }

        public Enemy()
        {
            mSpeed = 3;
            mMaxAcceleration = 5;
        }

        public virtual void LoadContent()
        {
            mCurrentAnim = 1;
            switch (mType)
            {
                case EnemyType.EXPLODING_ENEMY:
                    base.LoadContent(new int[] { 42, 43, 44, 43 }, new int[] { 42 });
                    mHealth = 50;
                    break;
                case EnemyType.CHARGING_ENEMY:
                    mHealth = 25;
                    base.LoadContent(new int[] { 39, 40, 41, 40 }, new int[] { 39 });
                    break;
                case EnemyType.FLYING_ENEMY:
                    base.LoadContent(new int[] { 8, 9, 10, 9 }, new int[] { 8 });
                    mHealth = 10;
                    break;
            }
        }

        public override void attack()
        {
            base.attack();
        }

        public override void update(Map map, GameTime time)
        {
            switch (mType)
            {
                case EnemyType.FLYING_ENEMY:
                    LerpToTarget(mTarget, 0.005f);
                    break;

                case EnemyType.EXPLODING_ENEMY:
                    explodeOnTarget(mTarget, 0.003f);
                    break;

                case EnemyType.CHARGING_ENEMY:
                    chargeTarget(mTarget, 0.02f);
                    break;
            };
            base.update(map, time);

            if (mIsExploding)
            {
                float transDelta = (float)(time.ElapsedGameTime.TotalMilliseconds / mTimeTillExplosion.TotalMilliseconds);
                if (transDelta == 1 || mHealth < 0)
                {
                    explode();
                }
            }
        }

        private void Swoop(Unit target, float time)
        {
            if (target == null)
                return;
				
				//Might be backwards
            Vector2 direction = target.Position - mPosition;
            float distance = direction.Length();
            Vector2 targetPos;

            if (distance > 460)
            {
                targetPos.X = ((float)Functions.random.Next(-1, 2) * 280) + mPosition.X;
                targetPos.Y = ((float)Functions.random.Next(-1, 2) * 280) + mPosition.Y;
            }
            else
            {
                targetPos.Y = target.Position.Y + distance;
                targetPos.X = target.Position.X + distance;
                if (direction.Y > 0)
                {
                    targetPos.Y = 0 - targetPos.Y;
                }
                if (direction.X > 0)
                {
                    targetPos.X = 0 - targetPos.X;
                }
            }

            mPosition.X = MathHelper.Lerp(mPosition.X, targetPos.X, time);
            mPosition.Y = MathHelper.Lerp(mPosition.Y, targetPos.Y, time);

        }

        private void explode()
        {
            //Do the explosion stuff here
            mIsDead = true;
        }

        public virtual void explodeOnTarget(Unit target, float timeToTarget)
        {
            if (target == null) return;
            Vector2 direction = target.Position - mPosition;
            float distance = direction.Length();
            Vector2 targetPos;

            if (distance > 660)
            {
                targetPos.X = ((float)Functions.random.Next (-1, 2) * 280) + mPosition.X;
                targetPos.Y = ((float)Functions.random.Next (-1, 2) * 280) + mPosition.Y;
            }
            else
                targetPos = target.Position;

            //lerp to target slowly, then explode on them
            mPosition.Y = MathHelper.Lerp(mPosition.Y, targetPos.Y, timeToTarget);
            mPosition.X = MathHelper.Lerp(mPosition.X, targetPos.X, timeToTarget);

            if ((distance < 128 || mHealth < 1) && !mIsExploding)
            {
                mIsExploding = true;
                mTimeTillExplosion = TimeSpan.FromSeconds(2.0f);
                //TODO work on timer
            }
        }

        public void rotateToFace(Unit target)
        {
            //Rotate to face the target
        }

        public void LerpToTarget(Unit target, float timeToTarget)
        {
            if (target == null) return;
            mPosition.Y = MathHelper.Lerp(mPosition.Y, target.Position.Y, timeToTarget);
            mPosition.X = MathHelper.Lerp(mPosition.X, target.Position.X, timeToTarget);
        }

        public void chargeTarget(Unit target, float timeToTarget)
        {
            if (target == null) return;
            //Lerp to the target quickly
           MathHelper.Lerp(this.mPosition.X, target.Position.X, 0.5f);
           MathHelper.Lerp(this.mPosition.Y, mPosition.Y, 0.5f);
            Vector2 direction = target.Position - mPosition;
            float distance = direction.Length();
            Vector2 targetPos;

            if (!(distance > 500))
            {
                if (mPosition.X < (target.Position.X + 64) && mPosition.X > (target.Position.X - 64))
                {
                    mPosition.Y = MathHelper.Lerp(mPosition.Y, target.Position.Y, timeToTarget);
                }
                else if (mPosition.Y < (target.Position.Y + 64) && mPosition.Y > (target.Position.Y - 64))
                {
                    mPosition.X = MathHelper.Lerp(mPosition.X, target.Position.X, timeToTarget);
                }
                else
                {
                    targetPos.X = ((float)Functions.random.Next(-1, 2) * 280) + mPosition.X;
                    targetPos.Y = ((float)Functions.random.Next(-1, 2) * 280) + mPosition.Y;
                    mPosition.Y = MathHelper.Lerp(mPosition.Y, targetPos.Y, timeToTarget);
                    mPosition.X = MathHelper.Lerp(mPosition.X, targetPos.X, timeToTarget);
                }
            }
            else
            {
                targetPos.X = ((float)Functions.random.Next(-1, 2) * 280) + mPosition.X;
                targetPos.Y = ((float)Functions.random.Next(-1, 2) * 280) + mPosition.Y;
                mPosition.Y = MathHelper.Lerp(mPosition.Y, targetPos.Y, timeToTarget);
                mPosition.X = MathHelper.Lerp(mPosition.X, targetPos.X, timeToTarget);
            }
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´