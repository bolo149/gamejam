﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Disadvantage
{
    public class Unit : Drawable
    {
        protected float mHealth = 100;
        protected Vector2 mDirection;
        protected Vector2 mVelocity = Vector2.Zero;
        protected Vector2 mSize; //Size compared to the tile
        protected float mSpeed;
        protected bool mIsDead;
        protected bool mIsAction;
        public bool mHitTarget = false;
        public bool mCanAttack = true;
        protected double mAttackCooldown;
        public double Cooldown
        {
            get { return mAttackCooldown; }
            set { mAttackCooldown = value; }
        }

        protected Unit mTarget;

        public Unit Target
        {
            get { return mTarget; }
            set { mTarget = value; }
        }

        public bool Dead
        {
            get { return mIsDead; }
            set { mIsDead = value; }
        }

        protected bool mIsInAir;
        public bool IsInAir
        {
            get { return mIsInAir; }
            protected set { mIsInAir = value; }
        }

        public Vector2 Velocity
        {
            get { return mVelocity; }

            set { mVelocity = value * mSpeed; }
        }

        public Unit()
        {
            // Add animations like so:
            //int[] sprites = { 0 };
            
            // Retrieving said animation
            //Animation idleAnim = getAnimation(0);
        }

        public virtual void attack()
        {
        }

        public virtual void hit(float damage)
        {
            mHealth -= damage;
            if (mHealth < 0)
                mIsDead = true;
        }

        public void checkHitTarget()
        {
            //if (mCanAttack)           
               // if(Collision.perPixelCollision(this, mTarget))
                   // mHitTarget = true;
        }

        public override void update(Map map, GameTime time)
        {
            //Idle is 0, moving is 1
            base.update(map, time);

            if (mTarget != null)
            {
                Room targetRoom = map.getCurrentRoom( mTarget.Position );
                if (!targetRoom.isInRoom(Position))
                    mTarget = null;
            }

            Vector2 newPos = new Vector2(mPosition.X + mVelocity.X, mPosition.Y);
            Tile tile = map.getTileAtXY(newPos + new Vector2(64, 100));
            if(tile != null && tile.Type != TileType.Solid &&
                tile.Type != TileType.Wall && tile.Type != TileType.Null)
            {
                if (tile.Type == TileType.Door)
                {
                    Room r = map.getCurrentRoom(mPosition);
                    if(r == null)
                        mPosition.X += mVelocity.X;
                    else
                    {
                        Door d = map.getClosestDoor(mPosition);
                        if (d.offset > 5)
                            mPosition.X += mVelocity.X;
                    }
                }
                else
                    mPosition.X += mVelocity.X;
            }

            newPos.Y -= mVelocity.Y;
            tile = map.getTileAtXY(newPos + new Vector2(64, 100));
            if (tile != null && tile.Type != TileType.Solid &&
                tile.Type != TileType.Wall && tile.Type != TileType.Null)
            {
                if (tile.Type == TileType.Door)
                {
                    Room r = map.getCurrentRoom(mPosition);
                    if(r == null)
                        mPosition.Y -= mVelocity.Y;
                    else
                    {
                        Door d = map.getClosestDoor(mPosition);
                        if (d.offset > 5)
                            mPosition.Y -= mVelocity.Y;
                    }
                }
                else
                    mPosition.Y -= mVelocity.Y;
            }

            checkHitTarget();
        }

        public virtual void LoadContent(int[] walkSprites, int[] idleSprites)
        {
            mAnimations.AddLast( new Animation( false, idleSprites ) );
            mAnimations.AddLast( new Animation( true, walkSprites ) );
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
