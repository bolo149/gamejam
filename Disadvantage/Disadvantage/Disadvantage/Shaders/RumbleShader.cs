﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    class RumbleShader : Shader
    {
        public Effect rumbler;

        public EffectParameter offset;
        public Vector2 offsetCoords;
        public float currentWeight;
        public float defaultWeight;

        public override void LoadContent(ContentManager content, GraphicsDevice device)
        {
            rumbler = content.Load<Effect>("Rumble");
            offset = rumbler.Parameters["offset"];
            defaultWeight = 0;// 100f;
            currentWeight = defaultWeight;
            offsetCoords = new Vector2((float)Functions.random.NextDouble() / currentWeight);
            offset.SetValue(offsetCoords);
            base.LoadContent(content, device);
        }

        public override void Update(GameTime gameTime)
        {
            if (currentWeight <= 0)
                isActive = false;
            else
                isActive = true;
            /*if (currentWeight < defaultWeight)
                currentWeight += 0.45f;*/
            Reset();
        }

        public float getWeight() { return currentWeight; }
        public void setWeight(float val) { currentWeight = val; }

        public override void Render(SpriteBatch batch)
        {
            batch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);

            rumbler.CurrentTechnique.Passes[0].Apply();      //XNA4.0
            batch.Draw(ShaderTexture, Vector2.Zero, Color.White);

            batch.End();
        }

        public override void Reset()
        {
            double rand = Functions.random.NextDouble();
            rand = (Functions.random.NextDouble() > 0.5) ? rand : rand * -1;
            offsetCoords.X = ((float)rand / currentWeight); rand = Functions.random.NextDouble();
            rand = (Functions.random.NextDouble() > 0.5) ? rand : rand * -1;
            offsetCoords.Y = ((float)rand / currentWeight);
            offset.SetValue(offsetCoords);
        }
    }
}
