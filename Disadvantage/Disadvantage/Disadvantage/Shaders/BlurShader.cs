﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    class BlurShader : Shader
    {
        public Effect blur;

        public EffectParameter distance;
        public float currentDistance;

        public override void LoadContent(ContentManager content, GraphicsDevice device)
        {
            blur = content.Load<Effect>("Blur");
            distance = blur.Parameters["BlurDistance"];
            currentDistance = 0;
            base.LoadContent(content, device);
        }

        public override void Update(GameTime time)
        {
            if (currentDistance > 0)
                currentDistance -= 0.0005f;
            else
                currentDistance = 0;

            distance.SetValue(currentDistance);
        }

        public void setDistance(float dist) { currentDistance = dist; }

        public override void Render(SpriteBatch batch)
        {
            batch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);

            blur.CurrentTechnique.Passes[0].Apply();      //XNA4.0
            batch.Draw(ShaderTexture, Vector2.Zero, Color.White);

            batch.End();

        }
    }
}
