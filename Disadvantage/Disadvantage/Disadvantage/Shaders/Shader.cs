﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    class Shader
    {
        public RenderTarget2D ShaderRenderTarget;
        protected Texture2D ShaderTexture;
        protected BlendState mBlendState;
        protected SamplerState mSamplerState;
        protected SpriteSortMode mSortMode;
        public bool isActive = true;

        public Shader()
        {
            mBlendState = BlendState.NonPremultiplied;
            mSamplerState = SamplerState.PointClamp;
            mSortMode = SpriteSortMode.Deferred;
        }

        public RenderTarget2D getRenderTarget() { return ShaderRenderTarget; }
        public Texture2D getTexture() { return ShaderTexture; }
        public void setTexture( RenderTarget2D target ) { ShaderTexture = target; }
        public BlendState getBlendState() { return mBlendState; }
        public SamplerState getSamplerState() { return mSamplerState; }
        public SpriteSortMode getSortMode() { return mSortMode; }
        
        public virtual void LoadContent(ContentManager content, GraphicsDevice device)
        {
            ShaderRenderTarget = Functions.CloneRenderTarget(device);
            ShaderTexture = new Texture2D(device, ShaderRenderTarget.Width, ShaderRenderTarget.Height);
        }

        public virtual void Update(GameTime time)
        {

        }

        public virtual void Reset()
        {

        }

        public virtual void Render(SpriteBatch batch)
        {
            batch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
            batch.Draw(ShaderTexture, Vector2.Zero, Color.White);
            batch.End();
        }
    }
}
