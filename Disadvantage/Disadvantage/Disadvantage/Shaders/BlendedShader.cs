﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.IO;

namespace Disadvantage
{
    public enum ShaderIDs
    {
        Blur = 0,
        Rumble
    }

    class BlendedShader : Shader
    {
        private List<Shader> mShaders;
        public BlendedShader()
        {
            mShaders = new List<Shader>();
            addShader(new BlurShader());
            addShader(new RumbleShader());
        }

        public override void LoadContent(ContentManager content, GraphicsDevice device)
        {
            foreach (Shader shader in mShaders)
                shader.LoadContent(content, device);
            base.LoadContent(content, device);
        }

        public override void Update(GameTime time)
        {
            foreach(Shader shader in mShaders)
                shader.Update(time);
        }

        public Shader getShader(int id) { return mShaders[id]; }

        public void addShader(Shader shader)
        {
            mShaders.Add(shader);
        }

        public void pass(int id, GraphicsDevice device, SpriteBatch batch)
        {
            RenderTargetBinding[] temp = device.GetRenderTargets();

            device.SetRenderTargets(mShaders[id].ShaderRenderTarget);
            batch.Begin();
            batch.Draw(ShaderRenderTarget, Vector2.Zero, Color.White);
            batch.End();
            mShaders[id].setTexture(mShaders[id].ShaderRenderTarget);

            device.SetRenderTargets(ShaderRenderTarget);
            mShaders[id].Render(batch);
            setTexture(ShaderRenderTarget);

            device.SetRenderTargets(temp);
        }

        
        public void render(GraphicsDevice device, SpriteBatch batch)
        {
            for (int i = 0; i < mShaders.Count; i++)
                if (mShaders[i].isActive) pass(i, device, batch);
            
            Render(batch);
        }
    }
}
