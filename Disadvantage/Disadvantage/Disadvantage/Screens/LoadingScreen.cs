﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Disadvantage
{
    class LoadingScreen : GameScreen
    {
        bool loadingIsSlow;
        bool otherScreensAreGone;

        GameScreen[] screensToLoad;

        private LoadingScreen(ScreenManager screenManager, bool loadingIsSlow, GameScreen[] screensToLoad)
        {
            this.loadingIsSlow = loadingIsSlow;
            this.screensToLoad = screensToLoad;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
        }

        public static void Load(ScreenManager screenManager, bool loadingIsSlow, PlayerIndex? controllingPlayer, params GameScreen[] screensToLoad)
        {
            // Tell all the current screens to transition off.
            foreach (GameScreen screen in screenManager.GetScreens())
                screen.exitScreen();

            // Create and activate the loading screen.
            LoadingScreen loadingScreen = new LoadingScreen(screenManager, loadingIsSlow, screensToLoad);

            screenManager.addScreen(loadingScreen, controllingPlayer);
        }

        public override void update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // If all the previous screens have finished transitioning
            // off, it is time to actually perform the load.
            if (otherScreensAreGone)
            {
                ScreenManager.removeScreen(this);

                foreach (GameScreen screen in screensToLoad)
                {
                    if (screen != null)
                    {
                        ScreenManager.addScreen(screen, ControllingPlayer);
                    }
                }

                // Once the load has finished, we use ResetElapsedTime to tell
                // the  game timing mechanism that we have just finished a very
                // long frame, and that it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }
        }

        public override void draw(GameTime gameTime)
        {
            // If we are the only active screen
            if ((ScreenState == ScreenState.Active) &&
                (ScreenManager.GetScreens().Length == 1))
            {
                otherScreensAreGone = true;
            }

            // The gameplay screen takes a while to load, so we display a loading
            // message while that is going on, but the menus load very quickly, and
            // it would look silly if we flashed this up for just a fraction of a second
            if (loadingIsSlow)
            {
                SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
                SpriteFont font = ScreenManager.Font;

                const string message = "Loading...";

                // Center the text in the viewport.
                Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
                Vector2 viewportSize = new Vector2(viewport.Width, viewport.Height);
                Vector2 textSize = font.MeasureString(message);
                Vector2 textPosition = (viewportSize - textSize) / 2;

                Color color = Color.White * TransitionAlpha;

                // Draw the text.
                spriteBatch.Begin();
                spriteBatch.DrawString(font, message, textPosition, color);
                spriteBatch.End();
            }
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
