﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Disadvantage
{
    class PauseMenuScreen : MenuScreen
    {

        public PauseMenuScreen()
            : base("OUR GAME TOO TUFF?")
        {
            MenuEntry resumeGame = new MenuEntry("Resume");
            MenuEntry mainMenu = new MenuEntry("MAIN MENU!");
            MenuEntry quitGame = new MenuEntry("I give up!");

            resumeGame.Selected += OnCancel;
            mainMenu.Selected += MainMenuSelected;
            quitGame.Selected += QuitGame;

            MenuEntries.Add(resumeGame);
            MenuEntries.Add(mainMenu);
            MenuEntries.Add(quitGame);
        }

        void QuitGame(object sender, PlayerIndexEventArgs e)
        {
            const string message = "Are you sure?";

            MessageBoxScreen confirmExitMessageBox = new MessageBoxScreen(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.addScreen(confirmExitMessageBox, e.PlayerIndex);
        }

        void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }

        void MainMenuSelected(object sender, PlayerIndexEventArgs e)
        {
            const string message = "Are you sure?";

            MessageBoxScreen confirmQuitMessageBox = new MessageBoxScreen(message);

            confirmQuitMessageBox.Accepted += ConfirmQuitMessageBoxAccepted;

            ScreenManager.addScreen(confirmQuitMessageBox, ControllingPlayer);
        }

        void ConfirmQuitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            SoundManager.stopSong();
            LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(), new MainMenuScreen());
        }

    }
}
