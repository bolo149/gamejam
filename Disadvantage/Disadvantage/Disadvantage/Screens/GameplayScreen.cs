﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Disadvantage
{
    class GameplayScreen : GameScreen
    {

        float mPauseAlpha;
        ContentManager mContent;
        SpriteFont mGameFont;
        Player mPlayer;
        Map mMap;
        Camera mCamera;
        UserInterface mInterface;
        BlendedShader mShader;
        int mGameLevel;
        Vector2 mLastPlrPos;
        double mBossTimer;
        bool mBossSpawned;

        public GameplayScreen(int gameLevel)
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            //TODO INITITITITIITITITTTTTTT (.)(.)
            mBossSpawned = false;
            mPlayer = new Player();
            mPlayer.Position = Vector2.Zero;
            mPlayer.Health = 10;
            mPlayer.mCanAttack = false;
            mShader = new BlendedShader();
            mCamera = new Camera( Functions.ScreenWidth, Functions.ScreenHeight);
            mInterface = new UserInterface(new Vector2(50, 50), new Vector2((Functions.ScreenWidth - 150), 50));
            mGameLevel = gameLevel;
        }

        public override void LoadContent()
        {
            if (mContent == null)
            {
                mContent = new ContentManager(ScreenManager.Game.Services, "Content");
            }
            SoundManager.LoadContent(mContent);

            //SoundManager.playSong(SoundManager.SongId.PLACEHOLDER_AMBIENCE);
            //SoundManager.playSoundLooped(SoundManager.SoundId.PLACEHOLDER_AMBIENCE);
            SoundManager.stopSong();

            mMap = new Map(400, 400, 64, mGameLevel);
            mMap.GenerateMap();
            SoundManager.playSound(SoundManager.SoundId.LEVEL_MUSIC);
            if (mMap.Difficulty > 0)
                mPlayer.mIsGunEquipped = true;
            if (mMap.Difficulty > 1)
            {
                mPlayer.pickupItem(CollectibleType.SPECIALGUN);
                SoundManager.playSound(SoundManager.SoundId.BOSS_MUSIC);
            }
            mCamera.Position = mMap.StartRoom.Position;
            mGameFont = mContent.Load<SpriteFont>("Font");
            mPlayer.LoadContent();
            mPlayer.Position = mCamera.Position;
            Vector2 temp = new Vector2(mCamera.Position.X - 100, mCamera.Position.Y);
            mLastPlrPos = mPlayer.Position;
            mBossTimer = 10000;
            UnitManager.LoadContent(mContent);
            UnitManager.setTarget((Unit)(mPlayer));
            CollectibleManager.setTarget(mPlayer);

            mShader.LoadContent(mContent, ScreenManager.GraphicsDevice);
            mInterface.LoadContent();
            //TODO LOAD SHEEETTT

            ScreenManager.Game.ResetElapsedTime();
        }

        public override void UnloadContent()
        {
            mContent.Unload();
        }

        public override void update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.update(gameTime, otherScreenHasFocus, false);


            if (mPlayer.Health <= 0) LoadingScreen.Load(ScreenManager, false, ControllingPlayer, new BackgroundScreen(), new MainMenuScreen()); 

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                mPauseAlpha = Math.Min(mPauseAlpha + 1f / 32, 1);
            else
                mPauseAlpha = Math.Max(mPauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                //What do you think goes here?! haha
                //TODO
                mPlayer.update(mMap, gameTime);
                UnitManager.update(mMap, gameTime, mPlayer);
                BulletManager.update(mMap, gameTime);
                ParticleManager.update(gameTime);
                CollectibleManager.update(mMap, gameTime);
                //mInterface.Update(gameTime, mPlayer);
                mBossTimer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                if (mBossTimer <= 0 && !mBossSpawned)
                {
                    // BOSS IS A GO
                    mBossSpawned = true;
                    int difficulty = mMap.Difficulty;
                    Boss boss = new Boss();
                    boss.Type = EnemyType.FLYING_BOSS + difficulty;
                    boss.LoadContent();
                    boss.Position = mMap.getCurrentRoom(mPlayer.Position).getRandomPoint();
                    UnitManager.add(boss);
                }

                if (mBossSpawned)
                {
                    bool bossAlive = false;
                    for (int unit = 0; unit < UnitManager.getNumUnits(); unit++)
                        if (UnitManager.get(unit) is Boss)
                            bossAlive = true;
                    if (!bossAlive && mPlayer.mIsGunEquipped)
                    {
                        LoadingScreen.Load(ScreenManager, false, ControllingPlayer, new BackgroundScreen(), new HUBScreen());
                    }
                }

                mShader.Update(gameTime);
            }

        }

        public override void handleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            PlayerIndex playerIndex = ControllingPlayer.Value;
            int currentPlayer = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[currentPlayer];
            GamePadState gamePadState = input.CurrentGamePadStates[currentPlayer];
            
            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected && input.GamePadWasConnected[currentPlayer];

            if (input.isPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                ScreenManager.addScreen(new PauseMenuScreen(), ControllingPlayer);

                //TODO PauseMenuScreen
            }
            else
            {
                Vector2 camPos = mCamera.Position;
                Vector2 tmpVel =  Vector2.Zero;
                float speed = 3;
                if (keyboardState.IsKeyDown(Keys.LeftShift))
                    speed *= 2;
                //Handle input here

                tmpVel = gamePadState.ThumbSticks.Left;

                //What if people don't have gamepads, WHAT THEN!?
                if (keyboardState.IsKeyDown(Keys.Down) || keyboardState.IsKeyDown (Keys.S))
                {
                    tmpVel.Y = -1;
                }
                if (keyboardState.IsKeyDown (Keys.Up) || keyboardState.IsKeyDown (Keys.W))
                {
                    tmpVel.Y = 1;
                }
                if (keyboardState.IsKeyDown (Keys.Left) || keyboardState.IsKeyDown (Keys.A))
                {
                    tmpVel.X = -1;
                }
                if (keyboardState.IsKeyDown (Keys.Right) || keyboardState.IsKeyDown (Keys.D))
                {
                    tmpVel.X = 1;
                }

                if (input.isNewButtonPress(Buttons.A, ControllingPlayer, out playerIndex) || input.isNewKeyPress(Keys.LeftAlt, ControllingPlayer, out playerIndex))
                {
                    mPlayer.attack();
                }
                if (input.isNewButtonPress(Buttons.B, ControllingPlayer, out playerIndex) || input.isNewKeyPress(Keys.Space, ControllingPlayer, out playerIndex))
                {
                    mPlayer.jetpack();
                }

                mPlayer.Velocity = tmpVel;
                mMap.updateDoors(mPlayer as Unit, ref mBossTimer);
                mPlayer.handleInput(gamePadState);
                mCamera.seek(mPlayer.Position + new Vector2(64,64));
                mMap.constrain(ref mCamera);
            }

        }

        public override void draw(GameTime gameTime)
        {

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            RenderTargetBinding[] tmp = ScreenManager.GraphicsDevice.GetRenderTargets();

            ScreenManager.GraphicsDevice.SetRenderTargets(mShader.ShaderRenderTarget);
            // Draw everything to the shader
            {
                ScreenManager.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 0, 0);
                spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null);

                //YEAH DRAW DAT SHEET!
                mMap.draw(spriteBatch, mCamera);
                BulletManager.draw(spriteBatch, mCamera.viewport());
                UnitManager.draw(spriteBatch, mCamera.viewport());
                CollectibleManager.draw(spriteBatch, mCamera.viewport());
                mPlayer.draw(spriteBatch, mCamera.viewport());

                //Draw this last \/ \/ \/
                //mInterface.Draw(spriteBatch, gameTime);

                spriteBatch.End();

                ParticleManager.draw(spriteBatch, mCamera.viewport());
            }
            ScreenManager.GraphicsDevice.SetRenderTargets(tmp);
            mShader.setTexture(mShader.ShaderRenderTarget);
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 0, 0);
            mShader.render(ScreenManager.GraphicsDevice, spriteBatch);

            if (TransitionPosition > 0 || mPauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, mPauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
