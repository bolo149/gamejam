﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Disadvantage
{
    //Enum numbers the screen state
    public enum ScreenState
    {
        TransitionOn,
        Active,
        TransitionOff,
        Hidden,
    }

    public abstract class GameScreen
    {

        //Specify if the screen is just a small popup
        public bool IsPopup
        {
            get { return mIsPopup; }
            protected set { mIsPopup = value; }
        }
        private bool mIsPopup = false;

        //Specify how long the screen takes to transition on
        public TimeSpan TransitionOnTime
        {
            get { return mTransitionOnTime; }
            protected set { mTransitionOnTime = value; }
        }
        private TimeSpan mTransitionOnTime = TimeSpan.Zero;

        //Specify how long the screen takes to transition off
        public TimeSpan TransitionOffTime
        {
            get { return mTransitionOffTime; }
            protected set { mTransitionOffTime = value; }
        }
        private TimeSpan mTransitionOffTime = TimeSpan.Zero;

        //Gets current position of transition from zero (fully active) to one (transitioned off)
        public float TransitionPosition
        {
            get { return mTransitionPosition; }
            protected set { mTransitionPosition = value; }
        }

        float mTransitionPosition = 1;

        //Gets current alpha of the screen transition (1 = fully active) (0 = off)
        public float TransitionAlpha
        {
            get { return 1f - TransitionPosition; }
        }

        //Gets the current transition state
        public ScreenState ScreenState
        {
            get { return mScreenState; }
            protected set { mScreenState = value; }
        }
        ScreenState mScreenState = ScreenState.TransitionOn;

        //Determines if the screen is exiting for real (Not just hiding)
        public bool IsExiting
        {
            get { return mIsExiting; }
            protected internal set { mIsExiting = value; }
        }
        bool mIsExiting = false;

        //Determine whether the screen can respond to user input.
        public bool IsActive
        {
            get
            {
                return !otherScreenHasFocus &&
                       (mScreenState == ScreenState.TransitionOn ||
                        mScreenState == ScreenState.Active);
            }
        }
        bool otherScreenHasFocus;

        /// Gets the manager that this screen belongs to.
        public ScreenManager ScreenManager
        {
            get { return screenManager; }
            internal set { screenManager = value; }
        }
        ScreenManager screenManager;

        //Gets the index of the controlling player, null if it's accepting from all input sources
        public PlayerIndex? ControllingPlayer
        {
            get { return mControllingPlayer; }
            internal set { mControllingPlayer = value; }
        }
        PlayerIndex? mControllingPlayer;

        //Load graphics content for the screen
        public virtual void LoadContent() { }

        //unload content for the screen
        public virtual void UnloadContent() { }

        public virtual void update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            this.otherScreenHasFocus = otherScreenHasFocus;

            if (mIsExiting)
            {
                //If the screen is dying it should transition off first
                mScreenState = ScreenState.TransitionOff;

                if (!updateTransition(gameTime, mTransitionOffTime, 1))
                {
                    //When the transition is done, remove the screen
                    ScreenManager.removeScreen(this); //TODO
                }
            }
            else if (coveredByOtherScreen)
            {
                //If the screen is covered transition off.
                if (updateTransition(gameTime, mTransitionOffTime, 1))
                {
                    //Still transitioning
                    mScreenState = ScreenState.TransitionOff;
                }
                else
                {
                    //Transition done
                    mScreenState = ScreenState.Hidden;
                }
            }
            else
            {
                //The screen is active and should transition on
                if (updateTransition(gameTime, mTransitionOnTime, -1))
                {
                    //Still transitioning
                    mScreenState = ScreenState.TransitionOn;
                }
                else
                {
                    //Done
                    mScreenState = ScreenState.Active;
                }
            }
        }

        //Updates the screen transition and position
        //Return true if transition false if it's done.
        bool updateTransition(GameTime gameTime, TimeSpan time, int direction)
        {
            //Howmuch to move by
            float transDelta;

            if (time == TimeSpan.Zero)
            {
                transDelta = 1;
            }
            else
            {
                transDelta = (float)(gameTime.ElapsedGameTime.TotalMilliseconds / time.TotalMilliseconds);
            }

            //Update the position
            mTransitionPosition += transDelta * direction;

            //Are we done?
            if ((direction < 0 && mTransitionPosition <= 0) || (direction > 0 && mTransitionPosition >= 1))
            {
                mTransitionPosition = MathHelper.Clamp(mTransitionPosition, 0, 1);
                return false;
            }

            //Strill transitioning
            return true;
        }

        //Allow the screen to handle input
        //Only called when the screen is active.
        public virtual void handleInput(InputState input) { }

        //Called when the screen should draw itself
        public virtual void draw(GameTime gameTime) { }

        //This method will respectfully kill the screen
        //Unlike the screen manager who has no respect
        public void exitScreen()
        {
            if (TransitionOffTime == TimeSpan.Zero)
            {
                // If the screen has a zero transition time, remove it immediately.
                ScreenManager.removeScreen(this);
            }
            else
            {
                // Otherwise flag that it should transition off and then exit.
                mIsExiting = true;
            }
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´