﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    class MenuScreen : GameScreen
    {
        List<MenuEntry> menuEntries = new List<MenuEntry>();
        int selectedEntry = 0;
        string menuTitle;

        protected IList<MenuEntry> MenuEntries
        {
            get { return menuEntries; }
        }

        public MenuScreen(string menuTitle)
        {
            this.menuTitle = menuTitle;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        public override void handleInput(InputState input)
        {
            // Move to the previous menu entry?
            if (input.isMenuUp(ControllingPlayer))
            {
                selectedEntry--;

                if (selectedEntry < 0)
                    selectedEntry = menuEntries.Count - 1;
            }

            // Move to the next menu entry?
            if (input.isMenuDown(ControllingPlayer))
            {
                selectedEntry++;

                if (selectedEntry >= menuEntries.Count)
                    selectedEntry = 0;
            }

            // Accept or cancel the menu? We pass in our ControllingPlayer, which may
            // either be null (to accept input from any player) or a specific index.
            PlayerIndex playerIndex;

            if (input.isMenuSelect(ControllingPlayer, out playerIndex))
            {
                OnSelectEntry(selectedEntry, playerIndex);
            }
            else if (input.isMenuCancel(ControllingPlayer, out playerIndex))
            {
                OnCancel(playerIndex);
            }

        }

        protected virtual void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[entryIndex].OnSelectEntry(playerIndex);
        }

        protected virtual void OnCancel(PlayerIndex playerIndex)
        {
            exitScreen();
        }

        protected void OnCancel(object sender, PlayerIndexEventArgs e)
        {
            OnCancel(e.PlayerIndex);
        }

        protected virtual void UpdateMenuEntryLocations()
        {
            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            // start at Y = 175; each X value is generated per entry
            Vector2 position = new Vector2(0f, 175f);

            // update each menu entry's location in turn
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                // each entry is to be centered horizontally
                position.X = ScreenManager.GraphicsDevice.Viewport.Width / 2 - menuEntry.GetWidth(this) / 2;

                if (ScreenState == ScreenState.TransitionOn)
                    position.X -= transitionOffset * 256;
                else
                    position.X += transitionOffset * 512;

                // set the entry's position
                menuEntry.Position = position;

                // move down for the next entry the size of this entry
                position.Y += menuEntry.GetHeight(this);
            }
        }


        public override void update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // Update each nested MenuEntry object.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                bool isSelected = IsActive && (i == selectedEntry);

                menuEntries[i].Update(this, isSelected, gameTime);
            }
        }

        public override void draw(GameTime gameTime)
        {
            // make sure our entries are in the right place before we draw them
            UpdateMenuEntryLocations();

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            // Draw each menu entry in turn.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                bool isSelected = IsActive && (i == selectedEntry);

                menuEntry.Draw(this, isSelected, gameTime);
            }

            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            // Draw the menu title centered on the screen
            Vector2 titlePosition = new Vector2(graphics.Viewport.Width / 2, 80);
            Vector2 titleOrigin = font.MeasureString(menuTitle) / 2;
            Color titleColor = Color.Red * TransitionAlpha;
            float titleScale = 2.25f;

            titlePosition.Y -= transitionOffset * 100;

            spriteBatch.DrawString(font, menuTitle, titlePosition, titleColor, 0,
                                   titleOrigin, titleScale, SpriteEffects.None, 0);

            spriteBatch.End();
        }

    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´