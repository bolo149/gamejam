﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Disadvantage
{
    class HUBScreen : MenuScreen
    {
        public HUBScreen()
            :base("HUB")
        {
            MenuEntry Level1Entry = new MenuEntry("First World");
            MenuEntry Level2Entry = new MenuEntry("Second World");
            MenuEntry BossEntry = new MenuEntry("Boss!");
            MenuEntry MainMenuEntry = new MenuEntry("Main Menu");

            Level1Entry.Selected += LevelOneEntrySelected;
            Level2Entry.Selected += LevelTwoEntrySelected;
            BossEntry.Selected += BossLevelSelected;
            MainMenuEntry.Selected += MainMenuSelected;

            MenuEntries.Add(Level1Entry);
            MenuEntries.Add(Level2Entry);
            MenuEntries.Add(BossEntry);
            MenuEntries.Add(MainMenuEntry);
        }

        void MainMenuSelected(object sender, PlayerIndexEventArgs e)
        {
            const string message = "Are you sure you want to quit? (You will lose any current progress)";

            MessageBoxScreen confirmQuitMessageBox = new MessageBoxScreen(message);

            confirmQuitMessageBox.Accepted += ConfirmQuitMessageBoxAccepted;

            ScreenManager.addScreen(confirmQuitMessageBox, ControllingPlayer);
        }

        void ConfirmQuitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            SoundManager.stopSong();
            LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(), new MainMenuScreen());
        }

        void LevelOneEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new GameplayScreen(0));
        }

        void LevelTwoEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new GameplayScreen(1));
        }

        void BossLevelSelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new GameplayScreen(2));
        }

        protected override void OnCancel(PlayerIndex playerIndex)
        {
            SoundManager.stopSong();
            LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(), new MainMenuScreen());
        }

    }
}
