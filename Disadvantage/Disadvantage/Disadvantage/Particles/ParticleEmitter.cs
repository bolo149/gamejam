﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public struct EmitterBehavior
    {
        public BlendState blending;
        public int maxParticles;
        public int numPerSpawn;
        public double spawnRate;
        public double lifeTime;
        public double rotationRate;
        public double topSpeedAge;
        public float scale;
        public float endScale;
        public Vector2 velocity;
        public Color startColor;
        public Color endColor;
        public Vector2 startAccel;
        public Vector2 stepAccel;
        public double fadeAge;

        public void reset()
        {
            blending = BlendState.Opaque;
            maxParticles = 50;
            spawnRate = 100;
            numPerSpawn = 1;
            lifeTime = 3000;
            rotationRate = 0;
            topSpeedAge = 1000;
            fadeAge = lifeTime;
            scale = 1;
            endScale = 1;
            velocity = startAccel = stepAccel = new Vector2(0, 0);
            startColor = Color.White;
            endColor = Color.White;
        }
    }

    public class ParticleEmitter
    {
        protected Vector2 mPosition;
        protected LinkedList<Particle> mParticles;
        protected double mAge;
        protected double mSpawnTime;
        protected EmitterBehavior mBehavior;
        protected int[] mSpriteIndices;
        protected double mEmitterLife;
        protected bool mCanSpawn = true;
        protected float mDepth = 1;
        public bool CanBeRemoved;
        private bool mOverride;

        public ParticleEmitter(bool over = false)
        {
            mSpriteIndices = new int[1];
            mSpriteIndices[0] = 2;

            mPosition = new Vector2(0, 0);
            mBehavior = new EmitterBehavior();
            mBehavior.blending = BlendState.NonPremultiplied;
            mParticles = new LinkedList<Particle>();

            mAge = 0;
            mEmitterLife = 10000;
            CanBeRemoved = false;
            mOverride = over;
        }

        public ParticleEmitter(Vector2 pos, double lifeTime, bool over = false)
        {
            mBehavior = new EmitterBehavior();
            mBehavior.blending = BlendState.NonPremultiplied;
            mParticles = new LinkedList<Particle>();

            mSpriteIndices = new int[1];
            mSpriteIndices[0] = 2;

            mAge = 0;
            mPosition = pos;
            mEmitterLife = lifeTime;
            mOverride = over;
            CanBeRemoved = false;
        }

        public ParticleEmitter(Vector2 pos, double lifeTime, EmitterBehavior behavior, bool over = false)
        {
            mParticles = new LinkedList<Particle>();

            mSpriteIndices = new int[1];
            mSpriteIndices[0] = 2;

            mAge = 0;
            mPosition = pos;
            mBehavior = behavior;
            mOverride = over;
            mEmitterLife = lifeTime;
            CanBeRemoved = false;
        }

        virtual public void update(GameTime time)
        {
            mAge += time.ElapsedGameTime.TotalMilliseconds;

            if(mSpawnTime > 0)
                mSpawnTime -= time.ElapsedGameTime.TotalMilliseconds;

            if (mSpawnTime <= 0 && mParticles.Count < mBehavior.maxParticles && mCanSpawn)
            {
                makeProjectile();
                mSpawnTime = mBehavior.spawnRate;
            }

            // Update Particles
            for (int i = 0; i < mParticles.Count; i++)
            {
                Particle p = mParticles.ElementAt<Particle>(i);
                p.update(time);
                if (p.CanBeRemoved)
                    mParticles.Remove(p);
            }

            // Update emitter
            if (mAge >= mEmitterLife && mEmitterLife > 0)
                mCanSpawn = false;
            if (mCanSpawn == false && mParticles.Count <= 0)
                CanBeRemoved = true;

            initBehavior();
        }
        
        public void setHorizonDepth( float horizon )
        {
            mDepth = ((mPosition.Y) - horizon) / (600 - horizon);
        }
        public void setDepth(float d)
        {
            mDepth = d;
        }

        public int getParticleCount()
        {
            return mParticles.Count;
        }

        public void setBehavior(EmitterBehavior behavior) { mBehavior = behavior; }
        public Vector2 getPosition() { return mPosition; }
        public void setPosition(Vector2 pos) { mPosition = pos; }
        public float getDepth() { return mDepth; }
        virtual public void initBehavior() { }

        virtual public void makeProjectile()
        {
            for (int i = 0; i < mBehavior.numPerSpawn; i++)
            {
                Animation anim = new Animation(true, mSpriteIndices);
                Particle p = new Particle(
                anim, // Sprite
                mPosition, //Position
                0, // Orientation
                mBehavior.scale, // Scale
                mBehavior.lifeTime);
                p.setScaling(mBehavior.scale,mBehavior.endScale);
                p.setVelocity(mBehavior.velocity, mBehavior.startAccel, mBehavior.stepAccel);
                p.setRotationRate((float)mBehavior.rotationRate);
                p.setTint(mBehavior.startColor, mBehavior.endColor);
                //p.setFade(0);

                mParticles.AddLast(p);
            }
        }

        public void draw(SpriteBatch batch, Vector2 offset)
        {
            batch.Begin(SpriteSortMode.Immediate, mBehavior.blending);
            for (int i = 0; i < mParticles.Count; i++)
                mParticles.ElementAt<Particle>(i).draw(batch, offset);
            batch.End();
        }
    }
}
