﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public class RadialBurst : ParticleEmitter
    {
        Color mColor;
        public RadialBurst(Vector2 pos, double lifeTime, Color c)
        {
            mSpriteIndices = new int[]{ 85 };

            mPosition = pos;
            mEmitterLife = lifeTime;
            mColor = c;
        }

        public override void initBehavior()
        {
            mBehavior.reset();
            mBehavior.blending = BlendState.Additive;
            mBehavior.startAccel = new Vector2(0, 0);
            mBehavior.stepAccel = new Vector2(0.09f, 0.09f);
            mBehavior.spawnRate = 300;
            mBehavior.numPerSpawn = 10;
            mBehavior.maxParticles = 10;
            mBehavior.scale = 4;
            mBehavior.endScale = 0.05f;
            mBehavior.lifeTime = 300;
            mBehavior.fadeAge = 0;
            mBehavior.startColor = mColor;
            Color endC = mColor;
            endC.A = 0;
            mBehavior.endColor = endC;
        }

        public override void makeProjectile()
        {
            float orient = 0;
            for (int i = 0; i < mBehavior.numPerSpawn; i++)
            {
                Animation anim = new Animation(true, mSpriteIndices);
                Particle p = new Particle(
                anim, // Sprite
                mPosition, //Position
                orient, // Orientation
                mBehavior.scale, // Scale
                mBehavior.lifeTime);
                p.setScaling(mBehavior.scale, mBehavior.endScale);

                mBehavior.velocity = Functions.AngleToVector(orient) / 8; // reduce speed
                p.setVelocity(mBehavior.velocity, mBehavior.startAccel, mBehavior.stepAccel);
                p.setFade(0);//mBehavior.fadeAge);
                p.setTint(mBehavior.startColor, mBehavior.endColor);
                mParticles.AddLast(p);

                orient += (float)((360 / mBehavior.numPerSpawn) * (Math.PI / 180));
            }
        }
    }

    class DirectionalBurst : ParticleEmitter
    {
        Color mColor;
        Vector2 mDirection;
        public DirectionalBurst(Vector2 pos, double lifeTime, Vector2 direction, Color c)
        {
            mSpriteIndices = new int[1];
            mSpriteIndices[0] = 0;
            mPosition = pos;
            mDirection = direction * -1;
            mEmitterLife = lifeTime;
            mColor = c;
        }

        public override void initBehavior()
        {
            mBehavior.reset();
            mBehavior.blending = BlendState.AlphaBlend;
            mBehavior.startAccel = new Vector2(1, 1);
            mBehavior.spawnRate = 10;
            mBehavior.numPerSpawn = 2;
            mBehavior.maxParticles = 20;
            mBehavior.scale = 2;
            mBehavior.endScale = 2;
            mBehavior.lifeTime = 500;
            mBehavior.startColor = mColor;
            mBehavior.endColor = mColor;
            // mBehavior.rotationRate = Functions.random.NextDouble() * Functions.random.Next(-2, 2);
        }

        public override void makeProjectile()
        {
            for (int i = 0; i < mBehavior.numPerSpawn; i++)
            {
                int offset = Functions.random.Next(-25, 25);
                float orient = Functions.VectorToAngle(mDirection) + (float)(offset * (Math.PI / 180));
                Animation anim = new Animation(true, mSpriteIndices);
                Particle p = new Particle(
                anim, // Sprite
                mPosition, //Position
                0, // Orientation
                mBehavior.scale, // Scale
                mBehavior.lifeTime);
                p.setScaling(mBehavior.scale, mBehavior.endScale);

                mBehavior.velocity = Functions.AngleToVector(orient);
                p.setVelocity(mBehavior.velocity, mBehavior.startAccel, mBehavior.stepAccel);
                //p.setRotationRate((float)mBehavior.rotationRate);
                p.setTint(mBehavior.startColor, mBehavior.endColor);
                mParticles.AddLast(p);
            }
        }
    }

    class RadialEmitter : ParticleEmitter
    {
        public RadialEmitter(Vector2 pos, double lifeTime)
        {
            mSpriteIndices = new int[1];
            mSpriteIndices[0] = 0;
            mPosition = pos;
            mEmitterLife = lifeTime;
        }

        public override void initBehavior()
        {
            mBehavior.reset();
            mBehavior.blending = BlendState.AlphaBlend;
            mBehavior.startAccel = new Vector2(1, 1);
            mBehavior.stepAccel = new Vector2(0.05f, 0.05f);
            mBehavior.spawnRate = 30;
            mBehavior.numPerSpawn = 6;
            mBehavior.maxParticles = 200;
            mBehavior.scale = Functions.random.Next(1,10);
            mBehavior.endScale = Functions.random.Next(1, 10);
            mBehavior.lifeTime = 600;
            mBehavior.startColor = Functions.RandomColor();
            mBehavior.endColor = mBehavior.startColor;
            // mBehavior.rotationRate = Functions.random.NextDouble() * Functions.random.Next(-2, 2);
        }

        public override void makeProjectile()
        {
            for (int i = 0; i < mBehavior.numPerSpawn; i++)
            {
                int offset = Functions.random.Next(-25, 25);
                Animation anim = new Animation(true, mSpriteIndices);
                Particle p = new Particle(
                anim, // Sprite
                mPosition, //Position
                0, // Orientation
                mBehavior.scale, // Scale
                mBehavior.lifeTime);
                p.setScaling(mBehavior.scale, mBehavior.endScale);

                float orient = (float)(Functions.random.Next(0, 360) * (Math.PI / 180));
                mBehavior.velocity = Functions.AngleToVector(orient);
                p.setVelocity(mBehavior.velocity, mBehavior.startAccel, mBehavior.stepAccel);

                p.setTint(mBehavior.startColor, mBehavior.endColor);
                mParticles.AddLast(p);
            }
        }
    }

    class LingerEmitter : ParticleEmitter
    {
        Color mColor;
        public LingerEmitter(Vector2 pos, double lifeTime, Color c)
        {
            mPosition = pos;
            mSpriteIndices = new int[] { 85 };
            mColor = c;
            mEmitterLife = lifeTime;
        }

        public override void initBehavior()
        {
            mBehavior.reset();
            mBehavior.blending = BlendState.AlphaBlend;
            mBehavior.startAccel = new Vector2(1, 1);
            //mBehavior.stepAccel = new Vector2(0.09f, 0.09f);
            mBehavior.velocity = Functions.AngleToVector((float)Functions.random.NextDouble()*6);
            mBehavior.spawnRate = 500;
            mBehavior.numPerSpawn = 3;
            mBehavior.maxParticles = 20;
            mBehavior.scale = 1;
            mBehavior.endScale = 0.05f;
            mBehavior.lifeTime = 200;
            mBehavior.fadeAge = 0;
            mBehavior.startColor = mColor;
            mBehavior.endColor = mColor;
        }

        public override void makeProjectile()
        {
            for (int i = 0; i < mBehavior.numPerSpawn; i++)
            {
                Animation anim = new Animation(true, mSpriteIndices);
                Particle p = new Particle(
                anim, // Sprite
                mPosition, //Position
                0, // Orientation
                mBehavior.scale, // Scale
                mBehavior.lifeTime);

                p.setScaling(mBehavior.scale, mBehavior.endScale);
                p.setVelocity(mBehavior.velocity / 13, mBehavior.startAccel, mBehavior.stepAccel);
                p.setFade(0);
                p.setTint(mBehavior.startColor, mBehavior.endColor);
                mParticles.AddLast(p);
            }
        }
    }

    class DirectionalEmitter : ParticleEmitter
    {
        Color mColor;
        Vector2 mDirection;
        public DirectionalEmitter(Vector2 pos, double lifeTime, Vector2 direction, Color c)
        {
            mSpriteIndices = new int[1];
            mSpriteIndices[0] = 0;

            mPosition = pos;
            mDirection = direction * -1;
            mEmitterLife = lifeTime;
            mColor = c;
        }

        public override void initBehavior()
        {
            mBehavior.reset();
            mBehavior.blending = BlendState.Additive;
            mBehavior.startAccel = new Vector2(1, 1);
            mBehavior.stepAccel = new Vector2(0.05f, 0.05f);
            mBehavior.spawnRate = 80;
            mBehavior.numPerSpawn = 5;
            mBehavior.maxParticles = 200;
            mBehavior.scale = 4;
            mBehavior.endScale = 8;
            mBehavior.lifeTime = 1600;
            mBehavior.startColor = Functions.RandomColor();
            mBehavior.endColor = mBehavior.startColor;
        }

        public override void makeProjectile()
        {
            for (int i = 0; i < mBehavior.numPerSpawn; i++)
            {
                int offset = Functions.random.Next(-25, 25);
                float orient = Functions.VectorToAngle(mDirection) + (float)(offset * (Math.PI / 180));
                Animation anim = new Animation(true, mSpriteIndices);
                Particle p = new Particle(
                anim, // Sprite
                mPosition, //Position
                0, // Orientation
                mBehavior.scale, // Scale
                mBehavior.lifeTime);
                p.setScaling(mBehavior.scale, mBehavior.endScale);

                mBehavior.velocity = Functions.AngleToVector(orient) / 2;
                p.setVelocity(mBehavior.velocity, mBehavior.startAccel, mBehavior.stepAccel);
                //p.setRotationRate((float)mBehavior.rotationRate);
                p.setTint(mBehavior.startColor, mBehavior.endColor);
                mParticles.AddLast(p);
            }
        }
    }
}
