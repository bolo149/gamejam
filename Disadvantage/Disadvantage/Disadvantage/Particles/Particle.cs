﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public class Particle
    {
        Animation mAnim;
        private Vector2 mPosition;
        private Vector2 mVelocity;
        private Vector2 mAccelFactor;
        private Vector2 mAcceleration;
        private Vector2 mOrigin;
        private Color mColor;
        private Color mStartColor;
        private Color mEndColor;
        private float mOrientation;
        private float mRotationRate;
        private float mScale;
        private float mStartScale;
        private float mEndScale;
        private double mFadeAge;
        private double mLifetime;
        private double mAge;
        private double mFadeFactor;
        public bool CanBeRemoved = false;

        public Particle(Animation anim, Vector2 position, float orient, float scale, double lifetime)
        {
            mAnim = anim;
            mPosition = position;
            mVelocity = new Vector2(0, 0);
            mAcceleration = mAccelFactor = new Vector2(0, 0);
            mOrigin = new Vector2(mAnim.getWidth() / 2, mAnim.getHeight() / 2);
            mColor = mStartColor = mEndColor = Color.White;
            mScale = mStartScale = mEndScale = scale;
            mOrientation = orient;
            mAge = mFadeFactor = 0;
            mFadeAge = mLifetime = lifetime;
            mRotationRate = 0;
        }

        public void setVelocity(Vector2 vel, Vector2 startAccel, Vector2 stepAccel)
        {
            mVelocity = vel;
            mAcceleration = startAccel;
            mAccelFactor = stepAccel;
        }
        public void setRotationRate(float rate) { mRotationRate = rate; }
        public void setScaling(float start, float end) { mScale = mStartScale = start; mEndScale = end; }
        public Vector2 getPosition() { return mPosition; }
        public void setTint(Color startColor, Color endColor)
        {
            mColor = mStartColor = startColor;
            mEndColor = endColor;
        }

        public void setFade(double fadeAge)
        {
            mFadeAge = fadeAge;
            mFadeFactor = ((mLifetime - mFadeAge) / 255);
        }

        public void update(GameTime time)
        {
            // Increase age
            mAge += time.ElapsedGameTime.TotalMilliseconds;

            // Change color
            mColor = Color.Lerp(mStartColor, mEndColor, (float)(mAge / mLifetime));

            // Change Rotation
            mOrientation += mRotationRate;

            // Change scale
            mScale = MathHelper.Lerp(mStartScale, mEndScale, (float)(mAge / mLifetime));

            // Change alpha
            if (mAge >= mFadeAge)
                mColor.A = (byte)MathHelper.Lerp(mStartColor.A, mEndColor.A, (float)((mAge-mFadeAge)/(mLifetime-mFadeAge)));

            // Manage speeds
            if( mAcceleration.X + mAccelFactor.X <= 1 )
                mAcceleration.X += mAccelFactor.X;
            if (mAcceleration.Y + mAccelFactor.Y <= 1)
                mAcceleration.Y += mAccelFactor.Y;
            mPosition.X += (mVelocity.X * mAcceleration.X) * (float)time.ElapsedGameTime.TotalMilliseconds;
            mPosition.Y += (mVelocity.Y * mAcceleration.Y) * (float)time.ElapsedGameTime.TotalMilliseconds;

            mAnim.update(time, 1);
            // Denote cleanup
            if (mAge >= mLifetime)
                CanBeRemoved = true;
        }

        public void draw(SpriteBatch batch, Vector2 offset)
        {
            mAnim.draw(batch, mPosition - offset, mOrientation, mOrigin, mScale, mColor);
        }
    }
}
