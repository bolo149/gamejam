﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public static class Collision
    {
        public static bool pointCollision(Vector2 point, Drawable target)
        {
            Rectangle rect = target.getBox();
            if (point.X > rect.X && point.X <= rect.X + rect.Width)
                if (point.Y > rect.Y && point.Y <= rect.Y + rect.Height)
                    return true;
            return false;
        }

        public static bool rectangleCollision(Drawable theFirstSprite, Drawable theSecondSprite)
        {
            Rectangle rectangleA;
            Rectangle rectangleB;
            rectangleA = theFirstSprite.getBox();
            rectangleB = theSecondSprite.getBox();//Gets the texture X,Y, Width, Height

            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);

            /*Proof of collision
             * if the highest top value is lower than the lowest bottom, they aren't colliding.
             * if the farthest left side is farther right than the farthest right side... they aren't colliding.
             * It's quite simple
             */
            if (top >= bottom || left >= right)
                return false;

            return true;
        }

        public static bool perPixelCollision(Drawable theFirstSprite, Drawable theSecondSprite)
        {
            //If they aren't colliding within a box, just stop
            if (!rectangleCollision(theFirstSprite, theSecondSprite))
                return false;

            Texture2D firstSpriteTexture = theFirstSprite.getSprite().getTexture();
            Texture2D secondSpriteTexture = theSecondSprite.getSprite().getTexture();

            //Rectangle are colliding so lets check the textures
            Color[] textureData1 = new Color[firstSpriteTexture.Width * firstSpriteTexture.Height];
            firstSpriteTexture.GetData(textureData1);

            Color[] textureData2 = new Color[secondSpriteTexture.Width * secondSpriteTexture.Height];
            secondSpriteTexture.GetData(textureData2);

            Rectangle rectangleA;
            Rectangle rectangleB;

            rectangleA = theFirstSprite.getBox();
            rectangleB = theSecondSprite.getBox();

            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);

            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    //Check potential intersecting pixels
                    Color colorA = textureData1[(x - rectangleA.Left) + (y - rectangleA.Top) * rectangleA.Width];
                    Color colorB = textureData2[(x - rectangleB.Left) + (y - rectangleB.Top) * rectangleB.Width];

                    //If neither of the current alphas for texture 1 and texture 2 are null (0) then they are colliding pixels
                    if (colorA.A != 0 && colorB.A != 0) return true;
                }
            }

            return false; //Lets just assume they aren't (I really don't like red underlines
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´