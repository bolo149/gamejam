﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public class Sprite
    {
        SpriteEffects mEffects = SpriteEffects.None;
        Texture2D mTexture;
        Rectangle mSourceRect;

        bool mIsSolid = false;
        public bool isSolid
        {
            get { return mIsSolid; }
            set { mIsSolid = value; }
        }

        double mFrameWaitTime;
        public double FrameTime
        {
            get { return mFrameWaitTime; }
            set { mFrameWaitTime = value; }
        }

        public Sprite(Texture2D tex, Rectangle srcRect)
        {
            mFrameWaitTime = 0;
            mTexture  = tex;
            mSourceRect = srcRect;
        }
        
        public Rectangle getSourceRect() { return mSourceRect; }
        public Texture2D getTexture() { return mTexture; }
        public void setEffects(SpriteEffects effect) { mEffects = effect; }
        public int getWidth() { return mSourceRect.Width; }
        public int getHeight() { return mSourceRect.Height; }

        public void draw(SpriteBatch batch, Vector2 pos, float orientation, Vector2 origin, float scale)
        {
            batch.Draw(mTexture, pos, mSourceRect, Color.White, orientation, origin, scale, mEffects, 1);
        }

        public void draw(SpriteBatch batch, Vector2 pos, float orientation, Vector2 origin, float scale, Color color)
        {
            batch.Draw(mTexture, pos, mSourceRect, color, orientation, origin, scale, mEffects, 1);
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´