﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    static class SpriteManager
    {
        static Texture2D mTexture;
        static Dictionary<int, Sprite> mSpriteList = new Dictionary<int, Sprite>();
        static private int mID = 0;
        static public int numFields = 6;

        public static void LoadContent(ContentManager content)
        {
            using( var listFile = TitleContainer.OpenStream("content\\SpriteList.txt") )
            {
                using(var sr = new StreamReader(listFile))
                {
                    int[] values = new int[numFields];
                    // 0 - source X
                    // 1 - source Y
                    // 2 - source W
                    // 3 - source H
                    // 4 - solid
                    // 5 - waitTime
                    string line;
                    while(!sr.EndOfStream)
                    {
                        line = sr.ReadLine();
                        // remove comments
                        int n = line.IndexOf('#');
                        if (n > 0)
                            line = line.Substring(0, n);

                        // get texture name
                        n = line.IndexOf(' ');
                        mTexture = content.Load<Texture2D>(line.Substring(0, n));
                        line = line.Substring(n + 1);


                        for (int i = 0; i < numFields; i++)
                        {
                            n = line.IndexOf(' ');
                            if (i != numFields-1)
                                int.TryParse(line.Substring(0, n), out values[i]);
                            else
                                int.TryParse(line, out values[i]);

                            line = line.Substring(n+1);
                        }
                        Rectangle sourceRect = new Rectangle(values[0], values[1], values[2], values[3]);
                        Sprite sp = new Sprite(mTexture, sourceRect);
                        sp.isSolid = (values[4] != 0) ? true : false;
                        sp.FrameTime = values[numFields-1];
                        add(sp);
                    }
                }
            }
        }

        public static void add(Sprite sprite)
        {
            mSpriteList.Add(mID, sprite);
            ++mID;
        }

        public static Sprite get(int id)
        {
            Sprite sp;
            mSpriteList.TryGetValue(id, out sp);
            return sp;
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´