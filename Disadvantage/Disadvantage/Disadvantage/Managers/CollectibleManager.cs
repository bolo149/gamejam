﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    enum CollectibleType
    {
        HEALTHBONE = 0,
        JETPACK = 1,
        LASERGUN = 2,
        SPECIALGUN = 3,
    }

    class CollectibleManager
    {
        static List<Collectible> mCollectibleList = new List<Collectible>();
        private static Player mTargetPlayer;

        public static void add(Collectible unit)
        {
            mCollectibleList.Add(unit);
        }

        public static void setTarget(Player player)
        {
            mTargetPlayer = player;
        }

        public static void update(Map map, GameTime gameTime)
        {
            checkCollisions(map);
            for (int i = 0; i < mCollectibleList.Count(); i++)
            {
                mCollectibleList.ElementAt(i).update(map, gameTime);
            }
            for (int j = 0; j < mCollectibleList.Count(); j++)
            {
                if (mCollectibleList.ElementAt(j).mPickedUp)
                {
                    ParticleManager.addEmitter(new LingerEmitter(mCollectibleList.ElementAt(j).Position, 500, new Color(255, 255, 0 ,255)));
                    mCollectibleList.RemoveAt(j);
                }
            }
        }

        public static void draw(SpriteBatch spriteBatch, Vector2 offset)
        {
            for (int i = 0; i < mCollectibleList.Count(); i++)
            {
                mCollectibleList.ElementAt(i).draw(spriteBatch, offset);
            }
        }

        public static void newCollectible(Vector2 position, CollectibleType type)
        {
            Collectible newCollectible = new Collectible( position, type);
            switch (type)
            {
                case CollectibleType.HEALTHBONE:
                    newCollectible.addAnimation(new Animation(false, new int[] { 77 }));
                    break;
                case CollectibleType.JETPACK:
                    newCollectible.addAnimation(new Animation(false, new int[] { 78 }));
                    break;
                case CollectibleType.LASERGUN:
                    newCollectible.addAnimation(new Animation(false, new int[] { 79 }));
                    break;
                case CollectibleType.SPECIALGUN:
                    newCollectible.addAnimation(new Animation(false, new int[] { 76 }));
                    break;
            }
            add(newCollectible);
        }

        public static Collectible get(int id)
        {
            Collectible ut = mCollectibleList.ElementAt(id);
            return ut;
        }

        public static void checkCollisions(Map map)
        {
            for (int i = 0; i < mCollectibleList.Count(); i++)
            {
                if(Collision.perPixelCollision(get(i), mTargetPlayer ))
                {
                    mCollectibleList.ElementAt(i).isBeingPickedUp();
                    mTargetPlayer.pickupItem(mCollectibleList.ElementAt(i).Type);
                }
            }
        }
    }
}
