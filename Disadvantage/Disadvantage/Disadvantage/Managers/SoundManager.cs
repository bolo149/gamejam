﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Media;



namespace Disadvantage
{
    

    public static class SoundManager
    {
        //When adding sounds add an enum here to represent their ID.
        public enum SongId
        {
            PLACEHOLDER_AMBIENCE = 0,
        }
        public enum SoundId
        {
            PLACEHOLDER_AMBIENCE = 0,
            LEVEL_MUSIC,
            LASER_SOUND,
            JETPACK_SOUND,
            TAZER_SOUND,
            BOSS_MUSIC,
        }
        private static Dictionary<SoundId, SoundEffect> mSounds;
        private static Dictionary<SongId, Song> mSongs;
        private static SoundId mCurSoundId;
        private static SongId mCurSongId;
        private static float mVolume;

        public static float Volume
        {
            get { return mVolume; }
            set { mVolume = value; }
        }

        public static void addSound(SoundEffect theSound)
        {
            mSounds.Add(mCurSoundId, theSound);
            mCurSoundId++;
        }

        public static void addSong(Song theSong)
        {
            mSongs.Add(mCurSongId, theSong);
            mCurSongId++;
        }

        public static Song getSong(SongId theID)
        {
            Song theEffect;
            mSongs.TryGetValue(theID, out theEffect);
            return theEffect;
        }

        public static SoundEffect getSound(SoundId theID)
        {
            SoundEffect theEffect;
            mSounds.TryGetValue(theID, out theEffect);
            return theEffect;
        }

        public static void playSong(SongId theID)
        {
            MediaPlayer.Stop();
            MediaPlayer.Play(getSong(theID));
        }

        public static void stopSong()
        {
            MediaPlayer.Stop();
        }

        public static void playSound(SoundId theID)
        {
            getSound(theID).Play(mVolume, 0.0f, 0.0f);
        }

        public static void playSoundLooped(SoundId theID)
        {
            SoundEffectInstance tmpInstance;
            tmpInstance = getSound(theID).CreateInstance();
            tmpInstance.IsLooped = true;
            tmpInstance.Play();
        }

        public static void LoadContent(ContentManager content)
        {
            mSounds = new Dictionary<SoundId, SoundEffect>();
            mSongs = new Dictionary<SongId, Song>();
            mCurSoundId = 0;
            mCurSongId = 0;
            mVolume = 1.0f;

            using (var listFile = TitleContainer.OpenStream("content\\SoundList.txt"))
            {
                using (var sr = new StreamReader(listFile))
                {
                    string line;
                    string assetName;
                    int type;
                    while (!sr.EndOfStream)
                    {
                        line = sr.ReadLine();
                        //Find the name
                        int n = line.IndexOf(' ');
                        assetName = line.Substring(0, n);

                        line = line.Substring(n + 1);
                        n = line.IndexOf(';');
                        int.TryParse (line.Substring (0, n), out type);
                        line = line.Substring(n + 1);

                        if (type == 1)
                        {
                            //Song
                            Song theSong = content.Load<Song>(assetName);
                            addSong(theSong);
                        }
                        else if (type == 0)
                        {
                            //soundEffect
                            SoundEffect theSound = content.Load<SoundEffect>(assetName);
                            addSound(theSound);
                        }
                    }
                }
            }
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
