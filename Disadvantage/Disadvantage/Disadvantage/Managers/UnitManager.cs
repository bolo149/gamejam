﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
	class UnitManager
	{
        static List<Unit> mUnitList = new List<Unit>();
        //static Dictionary<int, Unit> mUnitList = new Dictionary<int, Unit>();
        //static private int mID = 0;
        //static public int numFields = 6;

        public static void LoadContent(ContentManager content)
        {
        }

        public static void update(Map map, GameTime gameTime, Unit player)
        {
            for (int i = 0; i < mUnitList.Count(); i++)
            {
                mUnitList.ElementAt(i).update(map, gameTime);
                Unit unit = mUnitList.ElementAt(i);
                Room room = map.getCurrentRoom(player.Position);
                unit.Target = player;
                if (unit.mHitTarget)
                {
                    player.hit(1);
                }
                if( room != null && room.isInRoom(unit.Position) )
                    unit.update(map, gameTime);
            } 
            for (int j = 0; j < mUnitList.Count(); j++)
            {
                if (mUnitList.ElementAt(j).Dead)
                {
                    spawnHealthPickup(mUnitList.ElementAt(j).Position);
                    mUnitList.RemoveAt(j);
                }
            }
        }

        public static void draw(SpriteBatch spriteBatch, Vector2 offset)
        {
            for (int i = 0; i < mUnitList.Count(); i++)
            {
                mUnitList.ElementAt(i).draw(spriteBatch, offset);
            }
        }

        public static void add(Unit unit)
        {
            mUnitList.Add(unit);
        }

        public static void unitHit(int id, float damage)
        {
            mUnitList.ElementAt(id).hit(damage);
        }

        public static void setTarget(Unit target)
        {
            for (int i = 0; i < mUnitList.Count(); i++)
            {
                mUnitList.ElementAt(i).Target = target;
            }
        }

        public static int getNumUnits()
        {
            return mUnitList.Count();
        }

        public static Unit get(int id)
        {
            Unit ut = mUnitList.ElementAt(id);
            return ut;
        }

        public static void spawnHealthPickup(Vector2 pos)
        {
            int rand = Functions.random.Next(0, 5);
            if(rand == 3)
            {
                CollectibleManager.newCollectible(pos, CollectibleType.HEALTHBONE);
            }
            
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´