﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace Disadvantage
{
    public class InputState
    {
        public const int MaxInputs = 4;

        public readonly KeyboardState[] CurrentKeyboardStates;
        public readonly GamePadState[] CurrentGamePadStates;

        public readonly KeyboardState[] LastKeyboardStates;
        public readonly GamePadState[] LastGamePadStates;

        public readonly bool[] GamePadWasConnected;


        public InputState()
        {
            CurrentKeyboardStates = new KeyboardState[MaxInputs];
            CurrentGamePadStates = new GamePadState[MaxInputs];

            LastKeyboardStates = new KeyboardState[MaxInputs];
            LastGamePadStates = new GamePadState[MaxInputs];

            GamePadWasConnected = new bool[MaxInputs];
        }

        public void update()
        {
            for (int i = 0; i < MaxInputs; i++)
            {
                LastKeyboardStates[i] = CurrentKeyboardStates[i];
                LastGamePadStates[i] = CurrentGamePadStates[i];

                CurrentKeyboardStates[i] = Keyboard.GetState((PlayerIndex)i);
                CurrentGamePadStates[i] = GamePad.GetState((PlayerIndex)i);

                // Keep track of whether a gamepad has ever been
                // connected, so we can detect if it is unplugged.
                if (CurrentGamePadStates[i].IsConnected)
                {
                    GamePadWasConnected[i] = true;
                }
            }
        }


        public bool isNewKeyPress(Keys key, PlayerIndex? controllingPlayer, out PlayerIndex playerIndex)
        {
            if (controllingPlayer.HasValue)
            {
                // Read input from the specified player.
                playerIndex = controllingPlayer.Value;

                int i = (int)playerIndex;

                return (CurrentKeyboardStates[i].IsKeyDown(key) &&
                        LastKeyboardStates[i].IsKeyUp(key));
            }
            else
            {
                // Accept input from any player.
                return (isNewKeyPress(key, PlayerIndex.One, out playerIndex) ||
                        isNewKeyPress(key, PlayerIndex.Two, out playerIndex) ||
                        isNewKeyPress(key, PlayerIndex.Three, out playerIndex) ||
                        isNewKeyPress(key, PlayerIndex.Four, out playerIndex));
            }
        }

        public bool isNewButtonPress(Buttons button, PlayerIndex? controllingPlayer, out PlayerIndex playerIndex)
        {
            if (controllingPlayer.HasValue)
            {
                // Read input from the specified player.
                playerIndex = controllingPlayer.Value;

                int i = (int)playerIndex;

                return (CurrentGamePadStates[i].IsButtonDown(button) &&
                        LastGamePadStates[i].IsButtonUp(button));
            }
            else
            {
                // Accept input from any player.
                return (isNewButtonPress(button, PlayerIndex.One, out playerIndex) ||
                        isNewButtonPress(button, PlayerIndex.Two, out playerIndex) ||
                        isNewButtonPress(button, PlayerIndex.Three, out playerIndex) ||
                        isNewButtonPress(button, PlayerIndex.Four, out playerIndex));
            }
        }

        public bool isMenuSelect(PlayerIndex? controllingPlayer, out PlayerIndex playerIndex)
        {
            return isNewKeyPress(Keys.Space, controllingPlayer, out playerIndex) ||
                   isNewKeyPress(Keys.Enter, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.A, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.Start, controllingPlayer, out playerIndex);
        }

        public bool isMenuCancel(PlayerIndex? controllingPlayer, out PlayerIndex playerIndex)
        {
            return isNewKeyPress(Keys.Escape, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.B, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.Back, controllingPlayer, out playerIndex);
        }

        public bool isMenuUp(PlayerIndex? controllingPlayer)
        {
            PlayerIndex playerIndex;

            return isNewKeyPress(Keys.Up, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.DPadUp, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.LeftThumbstickUp, controllingPlayer, out playerIndex);
        }

        public bool isMenuDown(PlayerIndex? controllingPlayer)
        {
            PlayerIndex playerIndex;

            return isNewKeyPress(Keys.Down, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.DPadDown, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.LeftThumbstickDown, controllingPlayer, out playerIndex);
        }

        public bool isPauseGame(PlayerIndex? controllingPlayer)
        {
            PlayerIndex playerIndex;

            return isNewKeyPress(Keys.Escape, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.Back, controllingPlayer, out playerIndex) ||
                   isNewButtonPress(Buttons.Start, controllingPlayer, out playerIndex);
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´