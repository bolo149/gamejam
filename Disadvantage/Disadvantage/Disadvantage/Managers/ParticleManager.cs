﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    public static class ParticleManager
    {
        static LinkedList<ParticleEmitter> mEmitters = new LinkedList<ParticleEmitter>();

        public static void addEmitter(ParticleEmitter emitter)
        {
            mEmitters.AddLast(emitter);
        }

        public static void update(GameTime time)
        {
            for(int i=0; i < mEmitters.Count(); i++)
            {
                mEmitters.ElementAt(i).update(time);
                if(mEmitters.ElementAt(i).CanBeRemoved)
                {
                    mEmitters.Remove( mEmitters.ElementAt(i) );
                    i--;
                }
            }
        }

        public static void draw(SpriteBatch batch, Vector2 offset)
        {
            foreach (ParticleEmitter emitter in mEmitters)
                emitter.draw(batch, offset);
        }
    }
}
