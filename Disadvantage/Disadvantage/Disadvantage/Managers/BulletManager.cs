﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Disadvantage
{
    class BulletManager
    {
        static List<Bullet> mBulletList = new List<Bullet>();
        //static Dictionary<int, Bullet> mBulletList = new Dictionary<int, Bullet>();
        //static private int mID = 0;
        //static public int numFields = 6;

        public static void LoadContent(ContentManager content)
        {
            
        }

        public static void add(Bullet unit)
        {
            mBulletList.Add(unit);
        }

        public static void update(Map map, GameTime gameTime)
        {
            checkCollisions(map);
            for (int i = 0; i < mBulletList.Count(); i++)
            {
                mBulletList.ElementAt(i).update(map, gameTime);
            }
            for (int j = 0; j < mBulletList.Count(); j++)
            {
                if (mBulletList.ElementAt(j).mToRemove)
                {
                    ParticleManager.addEmitter(new RadialBurst(mBulletList.ElementAt(j).Position, 290, new Color(0,255,255,255)));
                    mBulletList.RemoveAt(j);
                }
            }
        }

        public static void draw(SpriteBatch spriteBatch, Vector2 offset)
        {
            for (int i = 0; i < mBulletList.Count(); i++)
            {
                mBulletList.ElementAt(i).draw(spriteBatch, offset);
            }
        }

        public static void newBullet(Vector2 velocity, Vector2 position, float damage)
        {
            Bullet newBullet = new Bullet(velocity, position, damage);
            add(newBullet);
        }

        public static Bullet get(int id)
        {
            Bullet ut = mBulletList.ElementAt(id);
            return ut;
        }

        public static void checkCollisions(Map map)
        {
            for (int i = 0; i < UnitManager.getNumUnits(); i++)
            {
                for (int j = 0; j < mBulletList.Count(); j++)
                {
                    if(Collision.perPixelCollision(UnitManager.get(i), get(j)))
                    {
                        UnitManager.unitHit(i, mBulletList.ElementAt(j).mDamage);
                        mBulletList.ElementAt(j).mToRemove = true;
                    }
                }
            }
            for (int b = 0; b < mBulletList.Count(); b++)
            {
                Tile tile = map.getTileAtXY(mBulletList[b].Position);
                if (Functions.isSolid(tile.Type))
                {
                    mBulletList[b].mToRemove = true;
                }
            }
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
