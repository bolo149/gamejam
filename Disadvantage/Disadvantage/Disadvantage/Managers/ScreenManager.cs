﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;

namespace Disadvantage
{
    public class ScreenManager : DrawableGameComponent
    {
        List<GameScreen> mScreens = new List<GameScreen>();
        List<GameScreen> mScreensToUpdate = new List<GameScreen>();

        InputState mInput = new InputState();

        SpriteBatch mSpriteBatch;
        SpriteFont mFont;
        Texture2D mBlankTexture;

        bool mIsInitialized;

        bool mTraceEnabled;

        //Stores a sprite batch for all game screens
        public SpriteBatch SpriteBatch
        {
            get { return mSpriteBatch; }
        }


        //Font shared by all other screens
        public SpriteFont Font
        {
            get { return mFont; }
        }


        //If true, a list of all the screens that exist and are being updated
        public bool TraceEnabled
        {
            get { return mTraceEnabled; }
            set { mTraceEnabled = value; }
        }

        //Construct a new screen manager
        public ScreenManager(Game game)
            : base(game)
        {
        }

        //Init the screen manager
        public override void Initialize()
        {
            base.Initialize();

            mIsInitialized = true;
        }

        //Load the graphics content here
        protected override void LoadContent()
        {
            ContentManager content = Game.Content;

            mSpriteBatch = new SpriteBatch(GraphicsDevice);
            mFont = content.Load<SpriteFont>("Font");
            mBlankTexture = content.Load<Texture2D>("blank");

            foreach (GameScreen screen in mScreens)
            {
                screen.LoadContent();
            }
        }

        //Unload the graphics content
        protected override void UnloadContent()
        {
            // Tell each of the screens to unload their content.
            foreach (GameScreen screen in mScreens)
            {
                screen.UnloadContent();
            }
        }

        //update the screens and allow them to react to input and run logic
        public override void Update(GameTime gameTime)
        {
            // Read the keyboard and gamepad.
            mInput.update();

            // Make a copy of the master screen list, to avoid confusion if
            // the process of updating one screen adds or removes others.
            mScreensToUpdate.Clear();

            foreach (GameScreen screen in mScreens)
                mScreensToUpdate.Add(screen);

            bool otherScreenHasFocus = !Game.IsActive;
            bool coveredByOtherScreen = false;

            // Loop as long as there are screens waiting to be updated.
            while (mScreensToUpdate.Count > 0)
            {
                // Pop the topmost screen off the waiting list.
                GameScreen screen = mScreensToUpdate[mScreensToUpdate.Count - 1];

                mScreensToUpdate.RemoveAt(mScreensToUpdate.Count - 1);

                // Update the screen.
                screen.update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

                if (screen.ScreenState == ScreenState.TransitionOn ||
                    screen.ScreenState == ScreenState.Active)
                {
                    // If this is the first active screen we came across,
                    // give it a chance to handle input.
                    if (!otherScreenHasFocus)
                    {
                        screen.handleInput(mInput);

                        otherScreenHasFocus = true;
                    }

                    // If this is an active non-popup, inform any subsequent
                    // screens that they are covered by it.
                    if (!screen.IsPopup)
                        coveredByOtherScreen = true;
                }
            }

            // Print debug trace?
            if (mTraceEnabled)
                traceScreens();
        }

        //Prints a list of the screens for debug purposes
        void traceScreens()
        {
            List<string> screenNames = new List<string>();

            foreach (GameScreen screen in mScreens)
                screenNames.Add(screen.GetType().Name);

            Debug.WriteLine(string.Join(", ", screenNames.ToArray()));
        }

        //Have screens draw themselves
        public override void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in mScreens)
            {
                if (screen.ScreenState == ScreenState.Hidden)
                    continue;

                screen.draw(gameTime);
            }
        }

        //Add a screen to the screen manager
        public void addScreen(GameScreen screen, PlayerIndex? controllingPlayer)
        {
            screen.ControllingPlayer = controllingPlayer;
            screen.ScreenManager = this;
            screen.IsExiting = false;

            // If we have a graphics device, tell the screen to load content.
            if (mIsInitialized)
            {
                screen.LoadContent();
            }

            mScreens.Add(screen);
        }

        //Removes a screen from the screen manager.
        //use GameScreen.ExitScreen instead so it doesn't dissapear instantly.
        public void removeScreen(GameScreen screen)
        {
            // If we have a graphics device, tell the screen to unload content.
            if (mIsInitialized)
            {
                screen.UnloadContent();
            }

            mScreens.Remove(screen);
            mScreensToUpdate.Remove(screen);
        }

        //Return an array copy of the screens
        public GameScreen[] GetScreens()
        {
            return mScreens.ToArray();
        }


        /// <summary>
        /// Helper draws a translucent black fullscreen sprite, used for fading
        /// screens in and out, and for darkening the background behind popups.
        /// </summary>
        public void FadeBackBufferToBlack(float alpha)
        {
            Viewport viewport = GraphicsDevice.Viewport;

            mSpriteBatch.Begin();

            mSpriteBatch.Draw(mBlankTexture,
                             new Rectangle(0, 0, viewport.Width, viewport.Height),
                             Color.Black * alpha);

            mSpriteBatch.End();
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´