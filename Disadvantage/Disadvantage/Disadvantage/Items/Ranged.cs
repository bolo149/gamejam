﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    class Ranged : Item
    {
        private float mDamage;
        private float mCoolDown;
        private float mCoolDownCounter;
        private bool mCanFire = true;
        public Ranged(float damage, float coolDown, int anim)
            : base()
        {
            mUsing = new Animation(false, new int[] { anim });
            mDamage = damage;
            mCoolDown = coolDown;
            mCoolDownCounter = mCoolDown;
            Origin = new Vector2(10, 110);
            InUse = false;
        }
        public Sprite getSprite()
        {
            return mUsing.getSprite();
        }
        public float CoolDown
        {
            get { return mCoolDown; }
            set { mCoolDown = value; }
        }

        public void fire(Vector2 bulletVelocity, Vector2 firePosition)
        {
            if (mCanFire)
            {
                mCanFire = false;
                mCoolDownCounter = mCoolDown;
                BulletManager.newBullet(bulletVelocity, firePosition, mDamage);
                SoundManager.playSound(SoundManager.SoundId.LASER_SOUND);
            }
        }

        public void update(GameTime time)
        {
            if (InUse)
            {
                mCoolDownCounter -= time.ElapsedGameTime.Milliseconds;
                if (mCoolDownCounter <= 0)
                {
                    mCanFire = true;
                }
                mUsing.update(time, 1);
            }
        }
    }
}

// 　　　　　　　　 　 　 　 ,, 、
// 　　　　　　　　　　　　/: : l　　,, -､
// 　　　　　　　 　 　 , : :' ':'└ノﾞ　:;;ﾘ
// 　　　　　　 　 　､'　 .: : : : : ミ .::;/　　　　　ﾍｯﾍｯﾍｯﾍｯ
// 　　 　 　 　 　 ノ゛ ゎ::.: :'¨ﾞ::.: :;八
// 　　 　 　 　 rｯ　　　 ,_　　　:: : : : ﾞ ,
// 　　 　 　 　 `ｩ--イヌﾞ　　　　:　　　' ,
// 　 　 　 　 　 (_／7´ 　 　 　 　　..:: : : ::､
// 　　　　　　　　　　ﾞ}　　　　　　,,.: : : : : : ゛゛ﾞﾞﾞ'' ､ _
// 　　　　　　　　　　 l,　 　 , : : : : :: : : : : : : : : : : : : : ‐- .
// 　　　　　　 　 　 　 l,　　' ': : : : : : : : : : : : : : : : : : : : : : : :ﾞ'.､
// 　　 　 　 　 　 　 　 ヾ　　: : : : : :.:;; : : : : : : : : : : : : : : : : : :.::.
// 　　　　　　　　　 　 　 丶　ﾞ ､: : : : :;;: : : : : : : : : : : : : : : : : ; ;}
// 　　　 　 　 　 　 　 　 　 _j　　l:　 　 ;;:: : : : : : : : : : : : : ; ; ; ;;ﾐ
// 　　　　　　　　　　 　 ',´, , ,,,ノ l:　　彡;;; ; ; ; ; : : : : :: ; ; ; ;;;ﾐ`
// 　 　 　 　 　 　 　 　 　 　 , -‐'ﾞ　 ｿﾞ ' ' ' ' ' ' ' ' ﾞ ﾞ ﾞ ﾞ ﾞ ﾞ´
// 　　　　　　　　　 　 　 　 　`' ' ' '´
