﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Disadvantage
{
    class Collectible: Drawable
    {
        public bool mPickedUp = false;
        CollectibleType mType;
        public Collectible(Vector2 position, CollectibleType type)
        {
            mType = type;
            mPosition = position;
        }

        public CollectibleType Type
        {
            get { return mType; }
            set { mType = value; }
        }

        public override void update( Map map, GameTime gameTime)
        {
            if (!mPickedUp)
            {
                base.update(map, gameTime);
            }

        }


        public override void draw(SpriteBatch batch, Vector2 pos)
        {
            if (!mPickedUp)
            {
                base.draw(batch, pos);
            }
        }

        public void isBeingPickedUp()
        {
            mPickedUp = true;
        }
    }
}
